<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package port-au-prince
 */

get_header(); ?>

	<div id="primary" class="content-area">
	    <main id="wp_main" class="site-main" role="main">

                <?php

		if ( is_front_page() || is_home() ) {
		    the_rp_issue();
                    $issue = get_latest_issue();
		?>
                        <div id="features">
                            <!-- Featured Items -->
                            <?php featured_articles($issue); ?>
                        </div>
                        <div id="main">
                            <!-- Dossier -->
                            <?php dossier($issue); ?>
                            <!-- Each other item -->
                            <?php other_articles($issue); ?>
                        </div>
                        <div id="side">
                            <!-- Editorial -->
                            <?php editorial($issue); ?>
                            <!-- Reviews -->
                            <?php reviews($issue); ?>
                            <!-- Featured Archive -->
                            <?php featured_archive($issue); ?>
                        </div>

                    <?php } else {
                        while ( have_posts() ) : the_post();

                        get_template_part( 'template-parts/content', 'page' );

                        endwhile; // End of the loop.
                    } ?>
		    <div id="po-block">
			<a class="po-item" href="https://www.radicalphilosophyarchive.com" target="_blank" title="A website dedicated to the first series of RP">Radical Philosophy Archive website<br>Series 1, 1972-2016</a>
		    </div>
                </main><!-- #main -->
        </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
