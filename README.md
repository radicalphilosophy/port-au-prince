# Radical Philosophy / Port-au-Prince Wordpress Theme

This is the theme for the
[https://www.radicalphilosophy.com](radicalphilosophy.com) Wordpress
site.

It is developed by a volunteer-run Engineering Collective who
**welcome** new commiters of all abilities with open arms provided
that they adhere to our [Code of Conduct](CODE-OF-CONDUCT).

The project employs HTML5, CSS3/SASS, JS and PHP.

## A bit of history

Launched as a paper journal in 1972, Radical Philosophy has operated
several iterations of its website at the
[radicalphilosophy.org](https://www.radicalphilosophy.org) domain. It
launched a static homepage [in
2000](https://web.archive.org/web/20000816223956/http://www.radicalphilosophy.com:80/),
migrating to a CMS in [March
2003](https://web.archive.org/web/20030321192657/http://radicalphilosophy.com/)
and a Wordpress site in [July
2011](https://web.archive.org/web/20110709064527/http://radicalphilosophy.com/),
with a theme named 'Frankfurt'. It featured PDFs of the entire
archive, although much was hidden behind a paywall. In [January
2018](https://web.archive.org/web/20180204000038/https://www.radicalphilosophy.com/),
after a year of reflection prompted by the publication of our 200th
issue, the journal relaunched for a second series; with it a new
theme, 'Port-au-Prince', a better indexed archive and no more paywall,
as well as a LaTeX-based production workflow. The Frankfurt site
exists as a snapshot [archive](https://radicalphilosophyarchive.com) which hosts the PDFs of issues 1-200.

In December 2018 'Port-au-Prince was released onto a public repo.

## Getting Started

Some broad tips on getting involved, to be expanded upon later. Our
strategy will be to get a new Wordpress install running and then
replace its database with the Radical Philosophy one. Note that this
install does not include images or PDFs, as they would make things way
too heavy. If you for need them to work on a feature, get in touch.

* Read our [Code of Conduct](CODE-OF-CONDUCT) and if you agree to
  abide by it, proceed fellow traveller. If not, farewell.
* Ensure you have a working Nginx/Apache-MySQL-PHP local
  environment. If you haven't, search the web for a guide along with
  your operating system.
* [Install `wp-cli`](https://make.wordpress.org/cli/handbook/installing/), the
  excellent Wordpress command line tool.
* Install git, so we can use it during installation.
* Specifically your webserver needs to serve a directory called
  rp-wordpress as root, relative to where you will run our installer
  script.
* Clone our rp-tools directory in the space where your webserver will
  host rp-wordpress. e.g. If you will have /var/www/rp-wordpress,
  clone into /var/www/.

```
$ cd /var/www
$ git clone https://gitlab.com/radicalphilosophy/rp-tools.git
```

* Now run our installations script.  Below you will find recommended
  parameters, that assume you will be serving on http://localhost
* If you prefer running the script with different settings, pass the
  --help flag for some terse info.

```
$ bash rp-tools/create-site.sh staging_rp staging_rp staging_rp 1
```

* That should be it. Of course, if you can't get there, do let us
  know. You can log in with the following username/passwords:

devadmin/devadmin

deveditor/deveditor

## Dependencies

### Laravel Mix 

This project uses Laravel Mix to build its css/scss and js files.

Make sure you have node and npm installed:

```
node -v
npm -v
```

Then from `/wp-content/themes/port-au-prince/` install dependencies:

```
npm install --save-dev webpack laravel-mix
```

After that you're ready to go.

For developer build of this project, where output is not minified,
run:

```
`npx mix`
```

For a developer build that auto-refreshes in the browser after every save:


```
npx mix watch
```

When you're ready to minify files for a production build, run:

```
npx mix --production
```

## Licence

[GNU General Public License v3.0](LICENSE)
