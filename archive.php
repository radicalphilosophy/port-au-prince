<?php
/**
 * The template for displaying outline of archive pages.
 * The actual entry templates are set by files in TEMPLATE-PARTS/
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package port-au-prince
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>

		    <header class="page-header">
		<?php
		if ( is_category() ) {
		    $item_title = single_cat_title( '', false ) . " archive";
		} elseif ( is_tag() ) {
		    $item_title = "'" . ucwords(single_cat_title( '', false )) . "' tag archive";
		} elseif (is_author()) {
		    $item_title = "Pieces by " . get_the_author();
		}
		echo '<h1 class="page-title">' . $item_title . '</h1>';
		?>
		    </header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

                        /* Is this an issue? */
                        if ( in_category("10") ) { ?>
                            <div class="single-issue">
                                <a href="<?php print(get_the_permalink()); ?>">
                                    <?php print(the_issue_image(get_the_id(), "medium")); ?>
                                    <h2>RP <?php print(get_styled_issue(get_the_title())); ?></h2>
                                    <p><?php print(get_issue_date(get_the_id())); ?></p>
                                </a>
                            </div>
                        <?php } elseif (is_author()) {
			    get_template_part( 'template-parts/content-author', get_post_format() );
			} else	{
                            /*
                             * Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             *
                             * We dispatch on 'content-archive' rather than just 'content' because we want a different
                             * look to that of the entire single post, since single.php invokes 'content'.
			     */
                            get_template_part( 'template-parts/content-archive', get_post_format() );
                        }
                        endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
