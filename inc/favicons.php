<?php function the_favis() {
    echo "/wp-content/themes/port-au-prince/img/favicon/";
}?>
<meta name="apple-mobile-web-app-title" content="Radical Philosophy"/>
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php the_favis(); ?>apple-touch-icon-57x57.png"/>
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php the_favis(); ?>apple-touch-icon-114x114.png"/>
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php the_favis(); ?>apple-touch-icon-72x72.png"/>
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php the_favis(); ?>apple-touch-icon-144x144.png"/>
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php the_favis(); ?>apple-touch-icon-60x60.png"/>
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php the_favis(); ?>apple-touch-icon-120x120.png"/>
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php the_favis(); ?>apple-touch-icon-76x76.png"/>
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php the_favis(); ?>apple-touch-icon-152x152.png"/>
<link rel="icon" type="image/png" href="<?php the_favis(); ?>favicon-196x196.png" sizes="196x196"/>
<link rel="icon" type="image/png" href="<?php the_favis(); ?>favicon-96x96.png" sizes="96x96"/>
<link rel="icon" type="image/png" href="<?php the_favis(); ?>favicon-32x32.png" sizes="32x32"/>
<link rel="icon" type="image/png" href="<?php the_favis(); ?>favicon-16x16.png" sizes="16x16"/>
<link rel="icon" type="image/png" href="<?php the_favis(); ?>favicon-128.png" sizes="128x128"/>
<meta name="application-name" content="Radical Philosophy"/>
<meta name="msapplication-TileColor" content="#3b3b3b"/>
<meta name="msapplication-TileImage" content="<?php the_favis(); ?>mstile-144x144.png"/>
<meta name="msapplication-square70x70logo" content="<?php the_favis(); ?>mstile-70x70.png"/>
<meta name="msapplication-square150x150logo" content="<?php the_favis(); ?>mstile-150x150.png"/>
<meta name="msapplication-wide310x150logo" content="<?php the_favis(); ?>mstile-310x150.png"/>
<meta name="msapplication-square310x310logo" content="<?php the_favis(); ?>mstile-310x310.png"/>
