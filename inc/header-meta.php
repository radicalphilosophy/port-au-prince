<?php
/**
 * Adds social metada to header
 *
 */
function the_rp_metadata() {
    $this_url            = get_permalink();
    $post                = get_post();
    $thing_title         = esc_html(strip_tags(get_the_title()));
    $thing_date_pub      = get_the_date('Y-m-d');

    $latest_issue_array  = get_latest_issue();
    $latest_issue_num    = $latest_issue_array->post_title;
    $latest_issue_date   = get_rpdate($latest_issue_array);
    $latest_issue_id     = $latest_issue_array->ID;
    $latest_issue_cover  = get_the_post_thumbnail_url($latest_issue_array->ID, 'large');
    $category            = single_cat_title( '', false );

    echo "\t<!-- Start of global meta tags -->\n";
    echo "\t\t<meta charset='UTF-8'>\n";
    echo "\t\t<meta name='viewport'                   content='width=device-width, initial-scale=1'>\n";
    echo "\t\t<meta property='og:site_name'           content='Radical Philosophy'>\n"; // FB
    echo "\t\t<meta name='twitter:site'               content='" . get_social_info()['twitter']['handle'] . "'>\n";
    echo "\t\t<meta name='citation_journal_title'     content='Radical Philosophy'>\n"; // Highwire Press for Google Scholar
    echo "\t\t<meta name='citation_issn'              content='0300-211X'>\n";
    echo "\t\t<meta property='og:url'                 content='$this_url'>\n";
    echo "\t\t<meta property='og:locale'              content='en_GB'>\n";
    echo "\t\t<meta name='twitter:card'               content='summary_large_image'>\n";

    if ( is_single() ) {
	/*
	 * This is an ISSUE or ITEM page
	 */
	$this_issue_array    = get_field('field_5918636bc19fe');
	$tags                = ucwords(single_cat_title('', false));
	$tag_array           = get_the_tags();

      	if ( in_category('issues') ) {
            /*
	     * This is an ISSUE page
	     */
	    $this_issue_id     = $latest_issue_array->ID;
	    $this_issue_num    = $latest_issue_array->post_title;
	    $issue_month          = get_field('issue_detail_date_month');
	    $issue_year           = get_field('issue_detail_date_year');
            $this_title        = "Radical Philosophy issue $thing_title ($issue_month $issue_year)";
	    if ( has_post_thumbnail() ) {
		$this_img = get_the_post_thumbnail_url($post->ID, 'medium');
	    } else {
		$this_img = get_site_url() . "/wp-content/files_mf/" . get_field('issue_detail_cover_image');
	    }

	    echo "\t\t<!-- Start of ISSUE meta tags -->\n";
            echo "\t\t<title>$this_title</title>\n";
            echo "\t\t<meta name='og:description'             content='Philosophical journal of the independent Left since 1972.'>\n";
            echo "\t\t<meta property='og:title'               content='$this_title'>\n";
            echo "\t\t<meta name='twitter:title'              content='$this_title'>\n";
            echo "\t\t<meta property='og:type'                content='website'>\n";
	    echo "\t\t<meta property='og:image'               content='$this_img'>\n";
            echo "\t\t<meta name='twitter:image'              content='$this_img'>\n";

	    // schema.org
	    $schema = array(
		"@context" => "https://schema.org",
		"@type" => "PublicationIssue",
		"name" => "$this_title",
		"headline" => "$this_title",
		"publication" => "Radical Philosophy",
		"issn" => "0300-211X",
		"issueNumber" => "$thing_title",
		"url" => "$this_url",
		"image" => array(
		    "$this_img"
		),
		"datePublished" => "$thing_date_pub",
		"inLanguage" => "en-GB",
		"licence" => "https://creativecommons.org/licenses/by-nc-nd/3.0/",
	    );

	    echo "<script type='application/ld+json'>" . json_encode($schema, JSON_UNESCAPED_SLASHES) . "</script>";
	    // end schema.org
      	} else {
	    /*
	     * This is an ITEM page
	     */
	    $this_issue_id     = $this_issue_array->ID;
	    $this_issue_num    = $this_issue_array->post_title;
	    $item_summary        = esc_html(get_field('item_detail_summary'));
	    $item_authors        = strip_tags(coauthors( null, null, null, null, false ));
	    $item_authors_array  = get_coauthors();
	    $issue_year          = get_field('field_58eaaaf246d9b', $this_issue_id);
	    $item_time_pub       = get_the_time('c');
	    $item_date_mod       = get_the_modified_date('c');
	    $item_subtitle       = strip_tags(limit_text(get_field('item_detail_sub_title'), 20)); // limit words because series 1 reviews used to put entire TOC of the issue's reviews into subtitle field!
	    $item_start_page = get_start_page();
	    $item_end_page = get_end_page();
	    $item_translator =  the_translator_links(null, null, false, false);
	    if ('' == $item_subtitle) {
		$item_title = "$thing_title";
	    } else {
		$item_title = "$thing_title: $item_subtitle";
	    }

	    if ( has_post_thumbnail() ) {
      		# If there is an image associated, use that
		$this_img = get_the_post_thumbnail_url($post->ID, 'medium');
	    } else {
      		# Else serve the issue cover
      		$this_img = get_issue_image($this_issue_id, 'large');
	    }

	    echo "\t\t<!-- Start of ITEM meta tags -->\n";
	    echo "\t\t<title>$item_authors · $item_title ($issue_year)</title>\n";
	    echo "\t\t<meta name='author'                     content='$item_authors'>\n";
	    echo "\t\t<meta property='og:type'                content='article'>\n";
	    echo "\t\t<meta property='article:title'          content='$item_authors · $item_title ($issue_year)'>\n";
	    echo "\t\t<meta property='og:title'               content='$item_authors · $item_title ($issue_year)'>\n";
	    echo "\t\t<meta property='article:published_time' content='$item_time_pub'>\n";
	    echo "\t\t<meta property='article:modified_time'  content='$item_date_mod'>\n";
	    echo "\t\t<meta name='twitter:title'              content='$item_authors · $item_title ($issue_year)'>\n";
	    echo "\t\t<meta property='og:image'               content='$this_img'>\n"; // Todo: debug

	    echo "\t\t<meta name='twitter:image'              content='$this_img'>\n"; // Todo: debug
	    if ($item_authors_array) {
		foreach($item_authors_array as $author) {
		    echo "\t\t<meta property='article:author'         content='$author->display_name'>\n";
		    echo "\t\t<meta name='citation_author'            content='$author->display_name'>\n";		}
	    }
            $this_summary = esc_html(limit_text(get_the_excerpt(), 40));
            echo "\t\t<meta name='description'                content='$this_summary'>\n";
            echo "\t\t<meta property='og:description'         content='$this_summary'>\n";
            echo "\t\t<meta name='twitter:description'        content='$this_summary'>\n";
	    
	    if ($tag_array) {
         	foreach($tag_array as $tag) {
         	    echo "\t\t<meta property='article:tag'            content='$tag->name'>\n";
         	}
	    }
	    // Highwire Press tags for Google Scholar indexing
	    echo "\t\t<meta name='citation_title'             content='$item_title'>\n";
	    echo "\t\t<meta name='citation_firstpage'         content='" . get_start_page() . "'>\n";
	    echo "\t\t<meta name='citation_lastpage'          content='" . get_end_page() . "'>\n";
	    echo "\t\t<meta name='citation_issue'             content='$this_issue_num'>\n";
	    echo "\t\t<meta name='citation_date'              content='$issue_year'>\n";//TODO: printing month
	    echo "\t\t<meta name='citation_fulltext_html_url' content='$this_url'>\n";
	    echo "\t\t<meta name='citation_public_url'        content='$this_url'>\n";
	    echo "\t\t<meta name='citation_keywords'          content='" . get_plain_tags('; ') . "'>\n";
	    echo "\t\t<meta name='citation_language'          content='en_GB'>\n";
	    echo "\t\t<meta name='citation_pdf_url'           content='" . get_pdf_url() . "'>\n";
	    echo "\t\t<meta name='DC.Rights'                  content='CC BY-NC-ND'>\n";

	    // schema.org
	    $schema = array(
		"@context" => "https://schema.org",
		"@type" => "ScholarlyArticle",
		"name" => "$item_title",
		"headline" => "$item_title",
		"publication" => "Radical Philosophy",
		"issn" => "0300-211X",
		"issueNumber" => "$this_issue_num",
		"url" => "$this_url",
		"image" => array(
		    "$this_img"
		),
		"translator" => array(
		    "@type" => "Person",
		    "name" => "$item_translator"
		),
		"pageStart" => "$item_start_page",
		"pageEnd" => "$item_end_page",
		"datePublished" => "$thing_date_pub",
		"inLanguage" => "en-GB",
		"licence" => "https://creativecommons.org/licenses/by-nc-nd/3.0/",
		"author" => []
	    );

	    // Add each author
	    $schema_authors = array();
	    foreach($item_authors_array as $author) {
		array_push($schema_authors, array(
		    "@type" => "Person",
		    "familyName" => "$author->last_name",
		    "givenName" => "$author->first_name",
		    "name" => "$author->display_name")
		);
	    };
	    $schema["author"] = $schema_authors;
	    echo "<script type='application/ld+json'>" . json_encode($schema, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . "</script>";
	    // end schema.org
        }
    } else {
	/*
	 * This must be the homepage or some kind of archive page
	 */
	echo "\t\t<!-- Start of MISC tags -->\n";
	echo "\t\t<meta property='og:type'                content='website'>\n";
	echo "\t\t<meta property='og:image'               content='$latest_issue_cover'>\n";
	echo "\t\t<meta name='twitter:image'              content='$latest_issue_cover'>\n";
        echo "\t\t<meta name='og:description'                content='Philosophical journal of the independent Left since 1972.'>\n";
	if ( is_front_page() ) {
	    $this_title = "Radical Philosophy issue $latest_issue_num ($latest_issue_date)";
	    echo "\t\t<!-- Start of FRONT PAGE meta tags -->\n";
	    echo "\t\t<title>$this_title</title>\n";
	    echo "\t\t<meta property='og:title'               content='$this_title'>\n";
	} elseif ( is_category() ) {
	    $this_title = "Radical Philosophy $category archive";
	    echo "\t\t<!-- Start of CATEGORY ARCHIVE meta tags -->\n";
	    echo "\t\t<title>$this_title</title>\n";
	    echo "\t\t<meta property='og:title'               content='$this_title'>\n";
	} elseif ( is_tag() ) {
	    $this_title = "Radical Philosophy $category archive";
	    echo "\t\t<!-- Start of TAG ARCHIVE meta tags -->\n";
	    echo "\t\t<title>$this_title</title>\n";
	    echo "\t\t<meta property='og:title'               content='$this_title'>\n";
	} elseif ( is_author() ) {
	    $this_title = "Radical Philosophy " . get_the_author() . " Archive";
	    echo "\t<!-- Start of CONTRIBUTOR PAGE meta tags -->\n";
	    echo "\t\t<title>$this_title</title>\n";
	    echo "\t\t<meta property='og:title' content='$this_title'>\n";
	} elseif ( $thing_title == "Contributor Directory" ) {
	    $this_title = 'Radical Philosophy Contributor Directory';
	    echo "\t<!-- Start of CONTRIBUTOR DIRECTORY meta tags -->\n";
    	    echo "\t\t<title>$this_title</title>\n";
	    echo "\t\t<meta property='og:title'               content='$this_title'>\n";
	} elseif ( is_page() ) {
	    $this_title = "Radical Philosophy $thing_title";
    	    echo "\t\t<!-- Start of PAGE meta tags -->\n";
	    echo "\t\t<title>$this_title</title>\n";
	} elseif ( is_search() ) {
	    $this_title = "Search: " . get_search_query() . " Radical Philosophy";
	    echo "\t\t<!-- Start of SEARCH PAGE meta tags -->\n";
	    echo "\t\t<title>$this_title</title>\n";
	} elseif ( is_404() ) {
	    $this_title = "404 error Radical Philosophy";
	    echo "\t<!-- Start of 404 PAGE meta tags -->\n";
	    echo "\t\t<title>$this_title</title>\n";
	} else {
	    $this_title =  'Radical Philosophy';
	    echo "\t<!-- Start of SOME OTHER meta tags -->\n";
	    echo "\t\t<title>$this_title</title>";
	}
    }
    echo "\t\t<!-- End of meta tags -->\n";
}
?>
