<?php

if ( !empty( $_FILES["file_upload"] ) ) {

    function clean($input){
	return implode("-", explode(" ", preg_replace("/[^a-zA-Z ]/", "", strtolower($input))));
    }

    $username = $_POST["username"];
    $file = $_FILES["file_upload"];
    $filename = $file["name"];
    $fileextension = pathinfo($filename, PATHINFO_EXTENSION);
    $filenametmp = $file["tmp_name"];
    $filetype = $file["type"];
    $filesize = $file["size"];
    $fileerror = $file["error"];

    $OUTPUT = "/tmp/" . basename("$filename", ".tex") . ".html";

    # Remove html file if exists
    exec("rm " . $OUTPUT);

    if ( $fileerror !== UPLOAD_ERR_OK ) {

	echo  "<p>Something went wrong with the file upload</p>";
	exit;

    } else {

	exec("sed -i " . $filenametmp . " -e 's/\\includegraphics\[[htbH]]*]/\includegraphics/g;' \
                                          -e 's/endnote{/footnote{/g;' \
                                          -e 's/\\begin{figure\*}\[[^]*\]//g;' \
                                          -e 's/translator{/textnormal{/g;' \
                                          -e 's/\\end{figure\\\*}//g;'");

	# Run pandoc of uploaded file into a new file as content
        $htmlfilenametmp = $filenametmp . "-" . time() . ".";
	exec(getenv("PANDOC_PATH") . " -f latex -t html -o " . $htmlfilenametmp . " " . $filenametmp);

	exec("sed -i " . $htmlfilenametmp . " -e 's/\[[ht]*\]//g;' \
                                              -e 's/<hr \/>/<h2 class=\"notes\">Notes<\/h2>/g;' \
                                              -e 's/<span>2<\/span>//g;' \
                                              -e 's/<p><\/p>//g;' \
                                              -e 's/<li id=\"fn/<li class=\"footnote\" id=\"fn/' \
                                              -e 's/class=\"emoji\"/class=\"reflink reffoot\"/g;' \
                                              -e 's/↩/^/g;' \
                                              -e 's/>^<\/a>/ class=\"reffoot footnoteLink\">^<\/a>/g' \
                                              -e 's/><sup>/>/g;' \
                                              -e 's/<\/sup></</g;' \
                                              -e 's/class=\"footnoteRef\"/class=\"footnoteRef footnoteLink\"/g;' \
                                              -e 's/<h2/<h3/g;'  \
                                              -e 's/<\/h2>/<\/h3>/g;' \
                                              -e 's/<img /<!--<img /g;' \
                                              -e 's/ height=\"[0-9]*\"//g;' \
                                              -e 's/ width=\"[0-9]*\"//g;' \
                                              -e 's/ alt=\"image\" \/>/\/>-->/g;' \
                                              -e 's/<p>[ ]*<!--/<!--/g;' \
                                              -e 's/-->[ ]*<\/p>/-->/g;' \
                                              -e 's/^<p><span class=\"nodecor\">/<p style=\"text-align: right\" class=\"translator\"><b>/g;' \
                                              -e 's_</span></p>\$_</b></p>_g;' \
                                            ");

	exec("cat " . $htmlfilenametmp . " >> " . $OUTPUT);

	# Serve complete tex file to the user
        if (file_exists($OUTPUT)) {

	    header("Content-Disposition: attachment; filename=\"" . basename($OUTPUT) . "\"");
	    header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
	    header("Cache-Control: public"); // needed for internet explorer
	    header("Content-Type: text/html");
	    readfile($OUTPUT);
	} else {
	    die("Error: File not found.");
	}
    }
}
?>
