<?php
/** User interface tweaks
 *
 *  We change admin menu items for ordinary editors to make the WP UI
 *  more intuitive.
 *
 *  This section does this.
 *
 */

// Remove some of the menu entries
function streamline_menu() {
    // Remove menu items with WP 3.1 and higher
    $current_user = wp_get_current_user();
    add_submenu_page(
        'edit.php', 'Browse Issues', 'Browse Issues', 'edit_pages' ,'/edit.php?category_name=issues'
    );
    add_submenu_page(
        'edit.php', 'Browse Dossiers', 'Browse Dossiers', 'edit_pages' ,'/edit.php?category_name=dossiers'
    );
    // Remove for all apart from named users
    if ( function_exists( 'remove_menu_page' ) && !$current_user->caps["administrator"] ) {
    }
}
add_action( 'admin_menu', 'streamline_menu' );
