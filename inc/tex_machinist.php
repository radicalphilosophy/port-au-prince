<?php

function tex_machinist(){
?>
    <div class="wrap">
        <h1><?= esc_html(get_admin_page_title()); ?></h1>
        <?php if ( isset($_POST['issue']) ) {
            /* Logic to process a new issue. */
            $issue = $_POST['issue'];

            /* Workstreams */
            /* We process new PDFs in distinct, well-defined steps. */
            /* These are: */
            /* WIP: FIXME: Currently disabled due to PDF upload issues. */
            /* $pdf = preprocess_pdf($issue, $_FILES['pdf_upload']); /* Shrink uploaded PDF */
            $details = add_issue($issue);                         /* Add WP content */
            /* WIP: FIXME: Currently disabled due to PDF upload issues. */
            /* attach_pdf($details, $pdf);                           /* Attach item pdfs */
            /* PDF processing is complete. */
            /* Now we generate the conclusions page. */

        ?>
            <p>Issue <?= $issue ?> was imported successfully</p>
            <p>For now you will need to:</p>
            <ol>
                <li>Add the cover image to the issue</li>
                <li>Manually create any dossier in this issue:</li>
                <ol>
                    <li>Create the dossier & associate it with the issue</li>
                    <li>Associate each article that is part of the dossier with it</li>
                </ol>
                <li>Set the featured articles for the issue</li>
                <li>Upload the PDF files for each article</li>
                <li>Set the featured image for each article</li>
            </ol>
        <?php } else { ?>
            <?php
            /* Logic to create machinist form. */
            $path = getenv("TEX_REPO");
            $nop = null;
            $return = 1;
            exec("cd " . $path . " && git pull", $nop, $return);
            $return == 0 || die("<p>Failed to establish a connection with Overleaf. Try again soon.</p><p>Git pull fail. The path to the LaTeX Git repository on this system is $path.");
            $dir = array_diff(
                scandir($path, 1),
                array('..', '.', ".git", "config", "help", "img-general")
            );
            $candidates = array_filter($dir, "issue_not_exists_p");
            asort($candidates);
            if ( empty($candidates) ) {
            ?>
                <p>No issues can currently be imported from Overleaf.</p>
		<p>It might be that I've already imported some of the issue you're trying to import, but not the whole thing. If that's the case then look at the <a href=\"/wp-admin/edit.php?post_type=post&all_posts=1&orderby=date&order=desc\">Posts panel</a> and see what the last post I imported was - the error will probably be in the metadata of the <em>next</em> tex file in Overleaf after that one.</p>
		<p>Typically this might be that the @pdf_page_start and @pdf_page_end page numbers have not been inputted, or that a line break has been inserted in a title or so on.</p>
		<p>Please try to fix any problems over at Overleaf, then delete whatever I've just imported from here in Wordpress, then delete everything permanently from the <a href=\"/wp-admin/edit.php?post_status=trash&post_type=post\">trash</a>, and then try again. Don't forget to delete any posts with the title of the issue number you're trying to import. If you're unable to get passed this error message then contact a website administrator.</p>
		<p>The Wordpress </p>
            <?php } else { ?>
                <form method="post" action="?page=tex_machinist" enctype="multipart/form-data">
	            <table class="form-table">
	                <tr>
		            <th scope="row">
                                <label for="issue">Which issue would you like to import? *</label>
                            </th>
		            <td>
                                <select name="issue" required="required">
                                    <?php $candidate = array_shift($candidates);  ?>
                                    <option value="<?= $candidate ?>" selected="selected"><?= $candidate ?></option>
                                    <?php foreach ( $candidates as $candidate ) { ?>
                                        <option value="<?= $candidate ?>"><?= $candidate ?></option>
                                    <?php } ?>
            <?php } ?>
                                </select>
                            </td>
	                </tr>
	                <!-- <tr>
		             <th scope="row">
                             <label for="pdf_upload">PDF file for issue *</label>
                             </th>
		             <td>
                             <input type="hidden" name="MAX_FILE_SIZE" value="128000000" />
		             <input name="pdf_upload" type="file" id="pdf_upload" required="required" />
                             </td>
	                     </tr> -->
	            </table>
                    <?php submit_button('Process issue');?>
        <?php } ?>
    </div>
<?php
}

function preprocess_pdf($issue, $pdf){
    $pdfname = $pdf["name"];
    $pdfextension = pathinfo($pdfname, PATHINFO_EXTENSION);
    $pdfnametmp = $pdf["tmp_name"];
    $pdftype = $pdf["type"];
    $pdfsize = $pdf["size"];
    $pdferror = $pdf["error"];
    $pdftargetname = "/tmp/rp" . $issue . "-" . time() . ".pdf";
    if ( $pdferror !== UPLOAD_ERR_OK ) {
        die("Something went wrong with the file upload");
    } elseif ( $pdfextension !== 'pdf' && $pdfextension !== 'PDF' ) {
        die("File is not a pdf: " . $pdfextension);
    }
    $nop = null;
    $return = 1;
    exec("gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 "
       . "-dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH "
       . "-sOutputFile=" . $pdftargetname . " "
       . $pdfnametmp, $nop, $return);
    $return == 0 || die("PDF conversion failed!");
    return $pdftargetname;
}

function issue_not_exists_p($issue){
    return (issue_exists_p($issue) ? false : true);
}

function issue_exists_p($issue){
    $args = array(
        'category_name' => 'issues',
        'title' => $issue,
        'post_status' => array(
            'publish', 'pending', 'draft', 'auto-draft', 'future',
            'private', 'inherit', 'trash'
        ),
    );
    return get_posts($args);
}

/* Create the issue & all individual items in wordpress.
   Returns an array, of assoc arrays; the latter containing WP posts &
   metadata for the post.  */
function add_issue($issue){
    $details = [];
    $details[] = tex_create_issue($issue);
    $path = getenv("TEX_REPO") . "/" . $issue . "/items/";
    $dir = array_diff(scandir($path), array('..', '.'));
    foreach ( $dir as $entry ) {
        $details[] = tex_create_post($issue, $path . $entry);
    }
    return $details;
}

function tex_san($field){
    $clean = preg_replace("/[^[:alnum:] ]/u", '', $field);
    return implode("_", explode(" ", $clean));
}

function pdf_name($issue, $author, $title){
    return "rp" . $issue . "-" . tex_san($author) . "-" . tex_san($title);
}

function cut_pdf($source_pdf, $first, $last, $issue, $author, $title){
    $filename = "/tmp/" . pdf_name($issue, $author, $title)  . ".pdf";
    $nop = null;
    $return = 1;
    $cmd = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4"
         . " -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -dFirstPage="
         . $first . " -dLastPage=" . $last . " -sOutputFile=" . $filename
         . " " . $source_pdf;
    exec($cmd, $nop, $return);
    if ( $return != 0 && $return != 134 ) {
        /* Excluding 134, because final PDF seems to error out on 134? */
        var_dump($source_pdf, $first, $last, $issue, $author, $title);
        var_dump($cmd);
        die("PDF cutting failed: " . $return);
    }
    return $filename;
}

function attach_pdf($details, $source_pdf){
    foreach ($details as $detail) {
        $meta = $detail['meta'];
        $pdf = cut_pdf($source_pdf, $meta['pdf_page_start'],
                       $meta['pdf_page_end'], $meta['issue'],
                       $meta['authors'][0], $meta['title']);
        $name = pdf_name($meta['issue'], $meta['authors'][0], $meta['title']);
        $type = "application/pdf";
        $post_id = $detail['ID'];
        $post_data = [];
        $desc = "PDF upload for " . $name;
        $file_array = [
            'name' => $name,
            'tmp_name' => $pdf,
            'type' => $type,
            'size' => filesize($pdf),
            'error' => 0
        ];
        $overrides = array(
	    // Tells WordPress to not look for the POST form
	    // fields that would normally be present as
	    // we downloaded the file from a remote server, so there
	    // will be no form fields
	    // Default is true
	    'test_form' => false,
	);
        $file = media_handle_sideload($file_array, $detail['ID'], $desc);
        // If error storing permanently, unlink
        if ( is_wp_error($file) ) {
	    @unlink($file_array['tmp_name']);
            die(var_dump($file));
        }
        die(var_dump($file));
        update_field('pdf_version', $file, $detail['ID']);
        die(var_dump($name));
    }
    return $details;
}

function tex_create_issue($issue){
    $args = array(
        'post_title' => $issue,
        'post_category' => array( get_cat_id('issues') ),
    );
    $ID = wp_insert_post($args, true);
    if ( is_wp_error($ID) ) {
        die("Issue creation failed");
    }
    update_field('issue_detail_date_year', date("Y"), $ID);
    update_field('issue_detail_date_month', date("F"), $ID);
    return [
        'ID' => $ID,
        'meta' => [
            'title' => 'Contents',
            'authors' => [ 'Radical Philosophy' ],
            'pdf_page_start' => 1,
            'pdf_page_end' => 1,
            'issue' => $issue
        ]
    ];
}

// user_login is the latex author/translator.  I can dedupe on that.
function user_does_not_exists_p($user){
    return !get_user_by("login", $user);
}

function tex_create_user($user){
    $user_array = explode(" ", $user);
    $args = array(
        'user_login' => $user,
        'user_email' => implode("_", $user_array) . "@noreply.radicalphilosophy.com",
        'user_nicename' => strtolower(implode("-", $user_array)),
        'last_name' => array_pop($user_array),
        'first_name' => implode(" ", $user_array),
        'user_pass' => wp_generate_password(),
        'role' => 'author',
    );
    if ( is_wp_error(wp_insert_user($args)) ) {
        die("User creation failed");
    }
}

function user_machinist($users) {
    $todo = array_filter($users, "user_does_not_exists_p");
    foreach ( $todo as $user ) {
        tex_create_user($user);
    }
}

function tex_create_post($issue, $path){
    $metadata = parse_metadata($issue, $path);
    $content = generate_contents($path);
    user_machinist($metadata['authors']);
    user_machinist($metadata['translators']);
    $args = array(
        'post_author' => $metadata['authors'][0],
        'post_content' => $content,
        'post_title' => $metadata['title'],
        'post_category' => array( get_cat_id($metadata['category']) ),
        'tags_input' => $metadata['tags'],
    );
    $ID = wp_insert_post($args, true);
    if ( is_wp_error($ID) ) {
        die("Post creation failed");
    }

    # Setup authors under co-author-plus
    global $coauthors_plus;
    $coauthors_plus->add_coauthors(
        $ID,
        array_map(
            function($author) {
                return get_user_by("login", $author)->user_nicename;
            },
            $metadata['authors']
        )
    );

    # Setup subtitle, issue number, start page, end page & translators
    update_field('item_detail_sub_title', $metadata['subtitle'], $ID);

    update_field(
        'item_issue_number',
        get_posts(
            array(
                'title' => $metadata['issue'],
                'category_name' => "issues",
                'post_status' => array( 'publish', 'draft'),
            )
        )[0],
        $ID);
    update_field('pdf_page_start', $metadata['pdf_page_start'], $ID);
    update_field('pdf_page_end', $metadata['pdf_page_end'], $ID);
    update_field(
        'item_translators',
        array_map(
            function($trans) {
                return get_user_by("login", $trans)->ID;
            },
            $metadata['translators']),
        $ID);

    update_field('item_author_biogs', $metadata['authorBiogs'], $ID);

    # If it is a review, setup altTitle
    if ( $metadata['category'] == "Review" ) {
        update_field('item_book_reviewed', $metadata['altTitle'], $ID);
    }

    return [
        'ID' => $ID,
        'meta' => $metadata
    ];
}

function vfail($author, $err){
    die("[ERROR]: In article by " . $author . ": " . $err);
}

function validate_metadata($metadata){
    isset($metadata['title'])
    || die($metadata['authors'][0] . ": @title is mandatary, but missing!");
    $author = $metadata['authors'][0];
    isset($metadata['authors'])
           || vfail($author, "@authors are mandatory, but missing!");
    isset($metadata['category'])
           || vfail($author, "@category is mandatory, but missing!");
    # PDF Page Validation
    isset($metadata['pdf_page_start'])
           || vfail($author, "@pdf_page_start is mandatory, but missing!");
    isset($metadata['pdf_page_end'])
           || vfail($author, "@pdf_page_end is mandatory, but missing!");
    $start = $metadata['pdf_page_start'];
    $end = $metadata['pdf_page_end'];
    ctype_digit($start)
        || vfail($author, "@pdf_page_start is not a page number!");
    ctype_digit($end)
        || vfail($author, "@pdf_page_end is not a page number!");
    ($start <= $end)
        || vfail($author, "@pdf_page_end is smaller than @pdf_page_start");

    isset($metadata['subtitle']) || $metadata['subtitle'] = "";
    isset($metadata['altTitle']) || $metadata['altTitle'] = "";
    isset($metadata['translators']) || $metadata['translators'] = array();
    isset($metadata['authorBiogs']) || $metadata['authorBiogs'] = "";
    isset($metadata['tags']) || $metadata['tags'] = array();

    return $metadata;
}

function parse_metadata($issue, $path){
    $metadata = array(
        'issue' => $issue,
    );
    $handle = fopen($path, "r");
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            /* Issue 202 fails because metadata does not conform to spec. */
            if (trim($line) == "%% METADATA_END:" || trim($line) == "%% @content:") {
                break;
            }
            $matches = array();
            if (preg_match("/^%% @([^ ]*)\b/", $line, $matches)) {
                switch ($matches[1]) {
                    case "category":        // Required
                    case "title":           // Required
                    case "subtitle":
                    case "altTitle":
		    case "authorBiogs":
                    case "pdf_page_start":  // Required
                    case "pdf_page_end":    // Required
                        $values = array();
                        preg_match("/{(.*)}/", fgets($handle), $values);
                        $value = latex_subset_parser($values[1]);
                        $metadata[$matches[1]] = $value;
                        break;
                    case "authors":    // Required
                    case "authorBiogs":
                    case "translators":
                    case "tags":
                        $values = array();
                        preg_match("/{(.*)}/", fgets($handle), $values);
                        $values = preg_split(
                            "/(, | and )/", $values[1], -1,
                            PREG_SPLIT_NO_EMPTY
                        );
                        $metadata[$matches[1]] =
                            array_map("latex_subset_parser", $values);
                        break;
                }
            }
        }
        fclose($handle);
    } else {
        die("Error opening file!");
    }
    return validate_metadata($metadata);
}

function latex_subset_parser($value){
    // latex newlines
    $value = preg_replace("/\\\\\\\\/", "", $value);
    // latex <i>
    $value = preg_replace("/\\\\emph{([^}]*)}/", "<i>$1</i>", $value);
    // latex <em>
    $value = preg_replace("/\\\\textit{([^}]*)}/", "<em>$1</em>", $value);
    // remove double whitespace anywhere
    $value = preg_replace("/[ ]{2,}/", " ", $value);
    return trim($value);
}

function generate_contents($path){
    $output = "";

    # First sed pass (over latex file)
    $lased = "sed " . $path
           . " -e 's/\\includegraphics\[[htbH]]*]/\includegraphics/g;' \
               -e 's/endnote{/footnote{/g;' \
               -e 's/\\begin{figure\*}\[[^]*\]//g;' \
               -e 's/translator{/textnormal{/g;' \
               -e 's/\\end{figure\\\*}//g;'";
    $output = `$lased`;

    $output = cmd("sed -n '/@content:/,\$p';", $output);

    # Conversion from $output -> html $output
    $pandoc = getenv("PANDOC_PATH");
    $cmd = $pandoc . " -f latex -t html5";
    $output = cmd($cmd, $output);

    # Second sed pass (over html)
    $htmlsed = "sed"
             . " -e 's/\[[ht]*\]//g;' \
                 -e 's/<hr \/>/<h2 class=\"notes\">Notes<\/h2>/g;' \
                 -e 's/<span>2<\/span>//g;' \
                 -e 's/<p><\/p>//g;' \
                 -e 's/<li id=\"fn/<li class=\"footnote\" id=\"fn/' \
                 -e 's/class=\"emoji\"/class=\"reflink reffoot\"/g;' \
                 -e 's/<a href=\"#fn/ <a href=\"#fn/g;' \
                 -e 's/↩/^/g;' \
                 -e 's/>^<\/a>/ class=\"reffoot footnoteLink\">^<\/a>/g' \
                 -e 's/><sup>/>/g;' \
                 -e 's/<\/sup></</g;' \
                 -e 's/class=\"footnoteRef\"/class=\"footnoteRef footnoteLink\"/g;' \
                 -e 's/<h2/<h3/g;'  \
                 -e 's/<\/h2>/<\/h3>/g;' \
                 -e 's/<img /<!--<img /g;' \
                 -e 's/ height=\"[0-9]*\"//g;' \
                 -e 's/ width=\"[0-9]*\"//g;' \
                 -e 's/ alt=\"image\" \/>/\/>-->/g;' \
                 -e 's/<p>[ ]*<!--/<!--/g;' \
                 -e 's/-->[ ]*<\/p>/-->/g;' \
                 -e 's/^<p><span class=\"nodecor\">/<p style=\"text-align: right\" class=\"translator\"><b>/g;' \
                 -e 's_</span></p>\$_</b></p>_g;'";
    $output = cmd($htmlsed, $output);

    # Return output
    return $output;
}

function cmd($cmd, $input){
    $descriptorspec = array(
        0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
        1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
        2 => array("file", "/tmp/error-output.txt", "a") // stderr is a file to write to
    );

    $process = proc_open($cmd, $descriptorspec, $pipes);

    if (is_resource($process)) {
        // $pipes now looks like this:
        // 0 => writeable handle connected to child stdin
        // 1 => readable handle connected to child stdout
        // Any error output will be appended to /tmp/error-output.txt

        fwrite($pipes[0], $input);
        fclose($pipes[0]);

        $output = stream_get_contents($pipes[1]);
        fclose($pipes[1]);

        // It is important that you close any pipes before calling
        // proc_close in order to avoid a deadlock
        $return_value = proc_close($process);
    }
    return $output;
}
