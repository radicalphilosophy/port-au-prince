<?php

acf_add_local_field_group(array(
    'key' => 'group_5c5ecad9376d0',
    'title' => 'Dossier metadata',
    'fields' => array(
	array(
	    'key' => 'field_5c5ec88asdjoa',
	    'label' => 'Note',
	    'name' => 'dossier_note',
	    'type' => 'message',
	    'instructions' => '',
	    'required' => 0,
	    'conditional_logic' => 0,
	    'wrapper' => array(
		'width' => '50',
		'class' => '',
		'id' => '',
	    ),
	    'message' => 'To add a dossier, just set the title above, which is the name of the dossier, and select which issue the dossier belongs to in the box to the right. The dossier will then appear in the "Is this a dossier?" dropdown list in the item admin page.',
	    'new_lines' => 'wpautop',
	    'esc_html' => 0,
	),
	array(
	    'key' => 'field_5918591dacf66',
	    'label' => 'Issue Number',
	    'name' => 'dossier_issue_number',
	    'type' => 'post_object',
	    'instructions' => '',
	    'required' => 0,
	    'conditional_logic' => 0,
	    'wrapper' => array(
		'width' => '50',
		'class' => '',
		'id' => '',
	    ),
	    'post_type' => array(
		0 => 'post',
	    ),
	    'taxonomy' => array(
		0 => 'category:issues',
	    ),
	    'allow_null' => 1,
	    'multiple' => 0,
	    'return_format' => 'object',
	    'ui' => 1,
	),
    ),
    'location' => array(
	array(
	    array(
		'param' => 'post_type',
		'operator' => '==',
		'value' => 'post',
	    ),
	    array(
		'param' => 'post_category',
		'operator' => '==',
		'value' => 'category:dossiers',
	    ),
	),
    ),
    'menu_order' => 0,
    'position' => 'acf_after_title',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => array(
	0 => 'permalink',
	1 => 'the_content',
	2 => 'excerpt',
	3 => 'discussion',
	4 => 'comments',
	5 => 'revisions',
	6 => 'slug',
	7 => 'author',
	8 => 'format',
	9 => 'page_attributes',
	10 => 'featured_image',
	11 => 'tags',
	12 => 'send-trackbacks',
    ),
    'active' => 1,
    'description' => '',
));

?>
