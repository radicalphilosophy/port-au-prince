<?php

// Load the machinist
require get_template_directory() . '/inc/tex_machinist.php';

/**
 * Register a custom menu page.
 */
function add_tex_machine_page(){
    add_menu_page(
        __( ' TeX Machine', 'textdomain' ),
        'TeX Machine',
        'publish_pages',
        'tex-machine',
        'tex_machine',
	'',
        2
    );
}
add_action( 'admin_menu', 'add_tex_machine_page' );

function add_tex_machine_submenus(){
    add_submenu_page(
	'tex-machine',                    // parent slug
	'Convert Word files into LaTeX',  // page title
	'Word to TeX',                    // menu title
	'publish_pages',                  // capability
	'tex-machine-to-tex',             // menu slug
	'tex_machine_to_tex'              // function
    );
    add_submenu_page(
	'tex-machine',                    // parent slug
	'Tex Machinist: import an issue', // page title
	'Overleaf Import',                // menu title
	'publish_pages',                  // capability
	'tex_machinist',                  // menu slug
        'tex_machinist'                   // function
    );
    add_submenu_page(
	'tex-machine',                    // parent slug
	'Convert LaTeX into HTML',        // page title
	'TeX to HTML file',               // menu title
	'publish_pages',                  // capability
	'tex-machine-to-html',            // menu slug
	'tex_machine_to_html'             // function
    );
}

add_action( 'admin_menu', 'add_tex_machine_submenus' );

/**
 * Register a TeX Machine widget
 */
function add_tex_machine_dashboard_widget(){
    wp_add_dashboard_widget(
	'tex_machine_dashboard_widget', // slug
	'TeX Machine',                  // title
	'tex_machine_widget_function'   // function to call
    );
}
add_action( 'wp_dashboard_setup', 'add_tex_machine_dashboard_widget' );

/**
 * Make the TeX Machine dashboard widget do something
 */
function tex_machine_widget_function(){
    echo "TeX Machine!!";
}

/**
 * Display TeX Machine primary menu page
 */
function tex_machine(){
?>
    <div class="wrap">
        <h1><?= esc_html(get_admin_page_title()); ?></h1>
	<img src="<?= get_template_directory_uri(); ?>/img/tex-lion-book-rp.png" />
    </div>
<?php
}

/**
 * Display TeX Machine primary submenu page
 */
function tex_machine_to_tex(){
?>
    <div class="wrap">
	<h1><?= esc_html(get_admin_page_title()); ?></h1>

	<form method="post" action="<?= get_template_directory_uri(); ?>/inc/do_tex_machine_to_tex.php" enctype="multipart/form-data">
	    <table class="form-table">
		<tr>
		    <th scope="row"><label for="category"><?php _e('Category *') ?></label></th>
		    <td><input name="category" type="text" id="category" value="<?php form_option('category'); ?>" class="regular-text" required/></td>
		</tr>
		<tr>
		    <th scope="row"><label for="title"><?php _e('Title *') ?></label></th>
		    <td><input name="title" type="text" id="title" value="<?php form_option('title'); ?>" class="regular-text" required/></td>
		</tr>
		<tr>
		    <th scope="row"><label for="subtitle"><?php _e('Subtitle ') ?></label></th>
		    <td><input name="subtitle" type="text" id="subtitle" value="<?php form_option('subtitle'); ?>" class="regular-text" /></td>
		</tr>
		<tr>
		    <th scope="row"><label for="authornames"><?php _e('Author names *') ?></label></th>
		    <td><input name="authornames" type="text" id="authornames" value="<?php form_option('authornames'); ?>" class="regular-text" required/></td>
		</tr>
		<tr>
		    <th scope="row"><label for="authorbiogs"><?php _e('Author biogs') ?></label></th>
		    <td><input name="authorbiogs" type="text" id="authorbiogs" value="<?php form_option('authorbiogs'); ?>" class="regular-text" /></td>
		</tr>
		<tr>
		    <th scope="row"><label for="tags"><?php _e('Tags * (separated by commas)') ?></label></th>
		    <td><input name="tags" type="text" id="tags" value="<?php form_option('tags'); ?>" class="regular-text" required/></td>
		</tr>
		<tr>
		    <th scope="row"><label for="translator"><?php _e('Translator(s)') ?></label></th>
		    <td><input name="translator" type="text" id="translator" value="<?php form_option('translator'); ?>" class="regular-text" /></td>
		</tr>
		<tr>
		    <th scope="row"><label for="file_input"><?php _e('File to be converted (NOT .doc)') ?></label></th>
		    <td><input name="MAX_FILE_SIZE" type="hidden" value="5242880" required/>
			<input name="file_upload" type="file" id="upload" /></td>
		</tr>
	    </table>
	    <input name="username" type="hidden" id="username" value="<?php $username = wp_get_current_user(); echo $username->user_firstname[0] . "" . $username->user_lastname[0];?>"/>
	    <?php submit_button( 'Generate TeX file' );?>
    </div>
<?php
}

function run_tex_machine_to_tex(){
    // see https://www.sitepoint.com/file-uploads-with-php/
}


function tex_machine_to_html(){
?>
    <div class="wrap">
	<h1><?= esc_html(get_admin_page_title()); ?></h1>

	<form method="post" action="<?= get_template_directory_uri(); ?>/inc/do_tex_machine_to_html.php" enctype="multipart/form-data">
	    <table class="form-table">
		<tr>
		    <th scope="row"><label for="file_input"><?php _e('.tex file to be converted') ?></label></th>
		    <td><input name="MAX_FILE_SIZE" type="hidden" value="5242880" />
			<input name="file_upload" type="file" id="upload" /></td>
		</tr>
	    </table>
            <?php submit_button( 'Generate HTML file' );?>
    </div>
<?php
}
