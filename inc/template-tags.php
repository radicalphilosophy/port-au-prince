<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package port-au-prince
 */

if ( ! function_exists( 'port_au_prince_posted_on' ) ) :
    /**
     * Prints HTML with meta information for the current post-date/time
     * and author.
     * We create our byline.
     */
    function port_au_prince_posted_on() {

    }
endif;

if ( ! function_exists( 'port_au_prince_entry_footer' ) ) :
    /**
     * Prints HTML with meta information for the categories, tags and comments.
     */
    function port_au_prince_entry_footer() {
        // Hide category and tag text for pages.
        if ( 'post' === get_post_type() && !in_category('issues') ) {
            /* translators: used between list items, there is a space after the comma */
            $categories_list = get_the_category_list( esc_html__( ', ', 'port-au-prince' ) );
        }

        if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
            echo '<span class="comments-link">';
            /* translators: %s: post title */
            comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'port-au-prince' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
            echo '</span>';
        }
    }
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function port_au_prince_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'port_au_prince_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'port_au_prince_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so port_au_prince_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so port_au_prince_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in port_au_prince_categorized_blog.
 */
function port_au_prince_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'port_au_prince_categories' );
}
add_action( 'edit_category', 'port_au_prince_category_transient_flusher' );
add_action( 'save_post',     'port_au_prince_category_transient_flusher' );
