<?php

if ( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
	'page_title' 	=> 'Site Options',
	'menu_title' 	=> 'Site Options',
	'menu_slug' 	=> 'site-options',
	'capability' 	=> 'edit_posts',
	'parent_slug'   => '',
	'position'      => 2,
	'icon_url'      => false,
	'redirect'      => false,
    ));

}

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
    'key' => 'group_5c9d07e60bf96',
    'title' => 'Alert banner (appears at top of website)',
    'fields' => array(
	array(
	    'key' => 'field_5c9d07f94f336',
	    'label' => 'Content (required)',
	    'name' => 'alert_banner_content',
	    'type' => 'textarea',
	    'instructions' => esc_html('Add links with a <a href="https://www.example.com/123">link text</a>. Add italics <i>like so</i>.'),
	    'required' => 0,
	    'conditional_logic' => 0,
	    'wrapper' => array(
		'width' => '',
		'class' => '',
		'id' => '',
	    ),
	    'default_value' => '',
	    'placeholder' => '',
	    'maxlength' => 240,
	    'rows' => 2,
	    'new_lines' => '',
	),
	array(
	    'key' => 'field_5c9d09dcd3701',
	    'label' => 'Publication start date (optional)',
	    'name' => 'alert_banner_start_date',
	    'type' => 'date_picker',
	    'instructions' => 'Banner will appear from this date on.',
	    'required' => 0,
	    'conditional_logic' => 0,
	    'wrapper' => array(
		'width' => '50',
		'class' => '',
		'id' => '',
	    ),
	    'display_format' => 'd/m/Y',
	    'return_format' => 'Ymd',
	    'first_day' => 1,
	),
	array(
	    'key' => 'field_5c9d0a01d3702',
	    'label' => 'Publication end date (required)',
	    'name' => 'alert_banner_end_date',
	    'type' => 'date_picker',
	    'instructions' => 'Banner will disappear <b>after</b> this date.',
	    'required' => 0,
	    'conditional_logic' => 0,
	    'wrapper' => array(
		'width' => '50',
		'class' => '',
		'id' => '',
	    ),
	    'display_format' => 'd/m/Y',
	    'return_format' => 'Ymd',
	    'first_day' => 1,
	),
	array(
	    'key' => 'field_5c9d124dea079',
	    'label' => 'Background color (optional)',
	    'name' => 'alert_banner_bg_color',
	    'type' => 'color_picker',
	    'instructions' => 'Press "clear" to reset to <span style="background-color: #78ffae;">#78ffae</span>.',
	    'required' => 0,
	    'conditional_logic' => 0,
	    'wrapper' => array(
		'width' => '33',
		'class' => '',
		'id' => '',
	    ),
	    'default_value' => '#78ffae',
	),
	array(
	    'key' => 'field_5c9d124gye763',
	    'label' => 'Text color (optional)',
	    'name' => 'alert_banner_text_color',
	    'type' => 'color_picker',
	    'instructions' => 'Press "clear" to reset to <span style="background-color: #000000;">#000000</span>.',
	    'required' => 0,
	    'conditional_logic' => 0,
	    'wrapper' => array(
		'width' => '33',
		'class' => '',
		'id' => '',
	    ),
	    'default_value' => '#000000',
	),
	array(
	    'key' => 'field_5c9d124xap298',
	    'label' => 'Link color (optional)',
	    'name' => 'alert_banner_a_color',
	    'type' => 'color_picker',
	    'instructions' => 'Press "clear" to reset  to <span style="background-color: #ff64b0;">#ff64b0</span>.',
	    'required' => 0,
	    'conditional_logic' => 0,
	    'wrapper' => array(
		'width' => '33',
		'class' => '',
		'id' => '',
	    ),
	    'default_value' => '#ff64b0',
	),
    ),
    'location' => array(
	array(
	    array(
		'param' => 'options_page',
		'operator' => '==',
		'value' => 'site-options',
	    ),
	),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

endif;

?>
