<?php

/** Author Creator
 *
 *  This implements functionality with which 'editor' level users can
 *  create 'author' users in an easy way, when creating the post.
 *
 */

// Add author-creator page to tools menu
function authc_options_page()
{
    add_management_page(
        'Author Creator Tool',
        'Author Creator',
        'manage_options',
        'author-creator',
        'authc_html'
    );
}

add_action('admin_menu', 'authc_options_page');

function authc_add_user($firstname, $lastname)
{
    $firstname = ucwords(strtolower($firstname));
    $lastname = ucwords(strtolower($lastname));
    $username = $firstname . " " . $lastname;
    $email = $firstname . "_" . $lastname . "@noreply.radicalphilosophy.com";
    $password = wp_generate_password($length=12);

    $userdata = [
        'password'    => $password,
        'user_login'  => $username,
        'user_email'  => $email,
        'first_name'  => $firstname,
        'last_name'   => $lastname,
        'description' => "Created using the author-creator tool.",
        'role'        => "author"

    ];
    return wp_insert_user($userdata);
}

function authc_html()
{
    $val = 0;
    if ( $_GET['first_name'] && $_GET['last_name'] ) {
        $val = authc_add_user($_GET['first_name'], $_GET['last_name']);
    }

    ?>
    <div class="wrap">
        <h1><?= esc_html(get_admin_page_title()); ?></h1>

        <p>
           <?php
           if ( is_wp_error($val) ) {
               echo "<p>" . $val->get_error_message() . "</p>";
           } elseif ($val) {
               echo "<p>" . "User created." . "</p>";
           }
           ?>
        </p>

        <p>Use this form to create new authors to be used when
            uploading articles.</p>

        <form method="get" action="/wp-admin/tools.php">
            <input type="hidden" id="page" name="page" value="author-creator"/>

            <table class="form-table">
                <tbody>
                    <tr class="form-field form-required">
                        <th scope="row">
                            <label for="first_name">
                                First Name <span class="description">(required)</span>
                            </label>
                        </th>
                        <td><input name="first_name" id="first_name" value="" type="text"></td>
                    </tr>
                    <tr class="form-field form-required">
                        <th scope="row">
                            <label for="last_name">
                                Last Name <span class="description">(required)</span>
                            </label>
                        </th>
                        <td><input name="last_name" id="last_name" value="" type="text"></td>
                    </tr>
	        </tbody>
            </table>
            <p class="submit">
                <input name="createuser" id="createusersub" class="button button-primary" value="Add New User" type="submit">
            </p>
        </form>

    <?php
}

?>
