<?php
$title = $_POST["title"];
$subtitle = $_POST["subtitle"];
$authors = $_POST["authornames"];
$authorbiogs = $_POST["authorbiogs"];
$category = $_POST["category"];
$lcategory = strtolower($category);
$tags = $_POST["tags"];
$username = $_POST["username"];
$translator = $_POST["translator"];

function clean($input){
    return implode("-", explode(" ", preg_replace("/[^a-zA-Z ]/", "", strtolower($input))));
} 

function esc_latex($input){
    # SIX is the precise number of slashes needed to generate three slashes in bash output,
    # as necessary for a single latex escape
    return preg_replace("/([{}])/", "\\\\\\1", $input);
}

# Name the output file after the final name of the last author
$authorlist = explode(' ', $authors);
$lastauthor = array_pop($authorlist);
$OUTPUT = "/tmp/" . clean($lastauthor) . ".tex";

# Remove tex file if exists
exec("rm " . $OUTPUT);

if ( !empty( $_FILES["file_upload"] ) ) {

    $file = $_FILES["file_upload"];
    $filename = $file["name"];
    $fileextension = pathinfo($filename, PATHINFO_EXTENSION);
    $filenametmp = $file["tmp_name"];
    $filetype = $file["type"];
    $filesize = $file["size"];
    $fileerror = $file["error"];
    
    if ( $fileerror !== UPLOAD_ERR_OK ) {
	
	echo  "<p>Something went wrong with the file upload</p>";
	exit;

    } else {
	
	# Create a tex file with form contents as preamble

        $latexhead = [
	    "%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",
            "%% METADATA\n\n",
	    "%% This section is fragile: changing the lines beginning with %% or moving about the {}'s (in which go your various settings) or anything beginning with @ may break things, so tread carefully!\n\n",
            (($lcategory == "review") or ($lcategory == "dossier")) ? "\Section\n" : "\Chapter\n",
	    "%% @category:\n{" . ucfirst(strtolower(esc_latex($category))) . "}\n",
	    "%% @title:\n{". esc_latex($title) ."}\n",
	    "%% @subtitle:\n{". esc_latex($subtitle) ."}\n",
	    "%% @authors:\n{". esc_latex($authors) ."}\n",
	    "%% @authorBiogs:\n{". esc_latex($authorbiogs) ."}\n",
            "%% @altTitle for table of contents:\n{}\n",
	    "%% @tags:\n{". esc_latex($tags) ."}\n",
	    "%% @translators:\n{". esc_latex($translator) ."}\n\n",
	    "\\exportMetadata\n",
	    "%% Special fields that are necessary for exporting this piece to the website.\n",
	    "%% @pdf_page_start:\n{}\n",
	    "%% @pdf_page_end:\n{}\n\n",
	    "%% METADATA_END",
	    "\n%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n",
            "%% @content:\n \n"
	];

        file_put_contents($OUTPUT, $latexhead);
	
	# Run pandoc of uploaded file into a new file as content
	$texfilenametmp = $filenametmp . "-" . time() . ".";
        exec(getenv("PANDOC_PATH") . " --wrap=none -f " . $fileextension . " -t latex -o " . $texfilenametmp . " " . $filenametmp);
	exec("cat " . $texfilenametmp . " >> " . $OUTPUT);
	
	# Close the document
        $latexfoot = [
	    ($lcategory == "review") ? "\n%% Insert the review author, properly formatted:\n\\reviewauthor\n" : "",
	    ($translator) ? "\n\\translator{" . esc_latex($translator) . "}\n" : "\n%% \\translator{}\n",
	    (($lcategory == "review") or ($lcategory == "obituary") or ($lcategory == "editorial")) ? "\n%% Uncomment the following to enable endnotes:\n% \Theendnotes" : "\n%% Comment the following to disable endnotes:\n\Theendnotes",
	    ($lcategory == "dossier") ? "\n%% Insert a manual page break at bottom of Dossier item:\n\Newpage" : "",
	    "\n\n%%%%% TeXMachine/TeX/". $username . "/" . date("j/m/Y/H:i:s") . " %%%%%\n"
        ];
	file_put_contents($OUTPUT, $latexfoot, FILE_APPEND);

	# Do any necessary find and replacing
	exec("sed -i " . $OUTPUT . " -e 's/footnote{/endnote{/g;'");
        
	# Serve complete tex file to the user
        if (file_exists($OUTPUT)) {

	    header("Content-Disposition: attachment; filename=\"" . basename($OUTPUT) . "\"");
	    header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
	    header("Cache-Control: public"); // needed for internet explorer
	    header("Content-Type: text/latex");
	    readfile($OUTPUT);
        } else {
	    die("Error: File not found.");
        } 
    }
}
?>
