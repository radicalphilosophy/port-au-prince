<?php

/** RP Abstractions
 *
 * We don't use the standard Wordpress content types.
 * We have content types that are built on "articles", but which are
 * customizations thereof through the addition of Advanced Custom
 * Fields.
 *
 * This file exposes high-level functions to operate on our content
 * types.
 *
 * Most functions below accept either a content title as a string, a
 * content ID as an integer or a content object as an object as a
 * parameter to the call.
 *
 * All procedures *should* return either a WP_Post object, or an array
 * of WP_Post objects.  The exception to this is get_dossier,
 * which is expected to return NULL if the issue has no dossier.
 *
 *
 * Examples:
 *
 * $issue = get_latest_issue() # Return our latest issue as WP_Post.
 * # Return the issue number (our issue titles are just their number)
 * $issue->post_title;
 * # Return a link to this issue
 * $issue->guid;
 *
 * # Return the dossier as WP_Post, associated with the issue or NULL
 * $dossier = get_dossier($issue);
 * # Return the dossier title
 * if ($dossier) {
 *     $dossier->post_title;
 * }
 * # Return the dossier posts (assuming $dossier is not NULL)
 * $posts = get_dossier_posts($dossier);
 *
 * # Return all interviews in our latest issue
 * $interviews = get_interviews(get_latest_issue());
 *
 * # Return an array of interview titles => urls
 * $metadata = array();
 * foreach ( $interviews as $wp_post ) {
 *     $metadata[$wp_post->post_title] = $wp_post->guid;
 * }
 *
 */

function in_to_object($in, $type) {
    $obj = false;
    if (is_string($in)) {
        # Assume we have the in title (number)
        $obj = get_posts([
            "numberposts" => 1,
            "category_name" => $type,
            "title" => $in
        ])[0];
    } elseif (is_integer($in)) {
        # Assume we have the in's post ID
        $obj = get_post($in);
    } elseif (is_object($in)) {
        # Assume we already have the in in question
        $obj = $in;
    } else {
        die("Expected either in title, in post id or in post object.");
    }
    return $obj;
}

function get_content($in, $type, $category_name, $meta_key) {
    $obj = in_to_object($in, $type);
    $posts = get_posts([
        "numberposts" => -1,
        "category_name" => $category_name,
        "meta_key" => $meta_key,
        "meta_value" => $obj->ID,
        "order" => "ASC",
        "orderby" => "date"
    ]);
    return $posts;
}

function get_series($issue) {
    if ( $issue->post_title > 200 ) {
        return "2";
    } else {
        return "1";
    }
}

function series($issue) {
    printf(get_series($issue));
}

function rpdate($issue) {
    printf(get_rpdate($issue));
}

function get_rpdate($issue) {
    $txt = get_field('field_58eaab453f8dd', $issue->ID) .
           " " .get_field('field_58eaaaf246d9b', $issue->ID);
    return $txt;
}

function rpitems($args, $xml) {
    $my_query = new WP_Query( $args );
    $print = false;
    while ( $my_query->have_posts() ) {
        $print = true;
        $my_query->the_post();
        $item = $xml->addChild("a");
        $item["class"] = 'item';
        $item["href"] = get_the_permalink();
        $img = $item->addChild("img");
        if ( has_post_thumbnail() ) {
            $img["src"] = get_the_post_thumbnail_url( '', 'medium');
        }
        $title = "<p>" . get_the_title() . "</p>";
        $item = appendElement($item, simplexml_load_string($title));
        $item->addChild("p", coauthors(null, null, null, null, null));
    }
    wp_reset_postdata();
    return $print;
}

// Glue to allow us to insert html fragments into existing html.
function appendElement($parent, $child) {
    // Create new DOMElements from the two SimpleXMLElements
    $parent = dom_import_simplexml($parent);
    $child  = dom_import_simplexml($child);
    // Import the child into the dictionary document
    $child  = $parent->ownerDocument->importNode($child, TRUE);
    // Append the child to parent in the dictionary
    $parent->appendChild($child);
    // return the newly augmented parent elemnet.
    return simplexml_import_dom($parent);
}

function reviewitems($args, $xml) {
    $my_query = new WP_Query( $args );
    $print = false;
    while ( $my_query->have_posts() ) {
        $print = true;
        $my_query->the_post();
        $item = $xml->addChild("a");
        $item["class"] = 'item';
        $item["href"] = get_the_permalink();
        $book = get_post_meta(get_the_ID(), "item_book_reviewed", true);
        if ( $book ) {
            $title = "<p>" . $book . "</p>";
        } else {
            $title = "<p>" . get_the_title() . "</p>";
        }
        $item = appendElement($item, simplexml_load_string($title));
        $item->addChild("p", coauthors(null, null, null, null, null));
    }
    wp_reset_postdata();
    return $print;
}

function get_latest_issue() {
    $issue = get_posts([
        "numberposts" => 1,
        "category_name" => "issues",
        "order" => "DESC",
        "orderby" => "date"
    ])[0];
    return $issue;
}

function get_dossier($issue) {
    $obj = in_to_object($issue, "issues");
    $dossier = get_posts([
        "numberposts" => 1,
        "category_name" => "dossiers",
        "meta_key" => "dossier_issue_number",
        "meta_value" => $obj->ID
    ]);
    return (empty($dossier) ? false : $dossier[0]);
}

function dossier($issue) {
    $xml = new SimpleXMLElement("<div id='dossier'></div>");
    $dossier = get_dossier($issue);
    if ( $dossier ) {
        $xml->addChild("h3", "Dossier: " . $dossier->post_title);
        $list = $xml->addChild('div');
        $list['class'] = 'item-group';
        /* Each Dossier item */
        $args = array(
            "numberposts" => -1,
            "category_name" => false,
            "meta_key" => "dossier_title",
            "meta_value" => $dossier->ID,
            "order" => "ASC",
            "orderby" => "date"
        );
        rpitems($args, $list);
        printf($xml->asXml());
    }
}

function get_dossier_posts($dossier) {
    return get_content($dossier, "dossiers", false, "dossier_title");
}

function get_reviews($issue) {
    return get_content($issue, "issues", "reviews", "item_issue_number");
}

function get_featured_articles($issue) {
    $obj = in_to_object($issue, "issues");
    $articles = get_posts([
        "numberposts" => 3,
        "meta_key" => "item_issue_number",
        "meta_value" => $obj->ID,
        "order" => "ASC",
        "orderby" => "date"
    ]);
    return $articles;
}

function featured_articles($issue) {
    $xml = new SimpleXMLElement("<div class='item-features'></div>");
    /* Each other-post item */
    $args = array(
        "numberposts" => 3,
        "meta_query" => array(
            "relation" => "and",
            "issue_clause" => array(
                'key' => "item_issue_number",
                'value' => $issue->ID,
            ),
            "featured_clause" => array(
                "key" => "item_detail_featured",
                "value" => "1",
            ),
        ),
        "order" => "ASC",
        "orderby" => "date"
    );
    $print = rpitems($args, $xml);
    if ( $print ) {
        printf($xml->asXml());
    }
}

function get_other_articles($issue) {
    $obj = in_to_object($issue, "issues");
    $articles = get_posts([
        "numberposts" => 50,
        "category_name" => "article",
        "meta_query" => array(
            "relation" => "and",
            "issue_clause" => array(
                'key' => "item_issue_number",
                'value' => $issue->ID,
            ),
            "featured_clause" => array(
                "key" => "item_detail_featured",
                "value" => "0",
            ),
        ),
        "order" => "ASC",
        "orderby" => "date"
    ]);
    return $articles;
}

function other_articles($issue) {
    $xml = new SimpleXMLElement("<div id='other-articles'></div>");
    $args = array(
        "numberposts" => -1,
        /* Exclude  dossier, individual-reviews, reviews & editorials */
        "cat" => array(-10091, -22, -10090, -73),
        "meta_query" => array(
            "relation" => "AND",
            array(
                'key' => "item_issue_number",
                'value' => $issue->ID,
            ),
            array(
                "relation" => "OR",
                array(
                    "key" => "item_detail_featured",
                    "value" => "",
                    "compare" => "NOT EXISTS",
                ),
                array(
                    "key" => "item_detail_featured",
                    "value" => "0",
                ),
            ),
            array(
                "relation" => "OR",
                array(
                    "key" => "dossier_title",
                    "compare" => "NOT EXISTS",
                ),
                array(
                    "key" => "dossier_title",
                    "value" => 0,
                    "compare" => "=",
                    "type" => "NUMERIC",
                ),
            ),
        ),
        "order" => "ASC",
        "orderby" => "date"
    );
    $print = rpitems($args, $xml);
    if ( $print ) {
        print($xml->asXml());
    }
}

function reviews($issue) {
    $xml = new SimpleXMLElement(
        "<div id='reviews'><h3>Reviews</h3></div>"
    );
    $list = $xml->addChild("div");
    $list['class'] = 'item-list';
    /* Each other-post item */
    $args = array(
        "numberposts" => -1,
        /* individual-reviews, reviews */
        "cat" => array(22, 10090),
        "meta_key" => "item_issue_number",
        "meta_value" => $issue->ID,
        "order" => "ASC",
        "orderby" => "date"
    );
    $print = reviewitems($args, $list);
    if ( $print ) {
        printf($xml->asXml());
    }
}

function editorial($issue) {
    $xml = new SimpleXMLElement(
        "<div id='editorial'><h3>Editorial</h3></div>"
    );
    /* Each other-post item */
    $args = array(
        "numberposts" => 1,
        "category_name" => 'editorial',
        "meta_key" => "item_issue_number",
        "meta_value" => $issue->ID,
        "order" => "ASC",
        "orderby" => "date"
    );
    $print = rpitems($args, $xml);
    if ( $print ) {
        printf($xml->asXml());
    }
}

function featured_archive($issue) {
    $xml = new SimpleXMLElement(
        "<div id='featured-archive'>
  <h3>
    <a href='https://www.radicalphilosophyarchive.com' style='text-decoration:none'>
      From the Archive
    </a>
  </h3>
</div>"
    );
    printf($xml->asXml());
}

# I'm not clear on what we should do about our other content types:
# - commentary
# - conference-report
# - contents
# - editorial
# - extras
# - interview
# - news
# - obituary
#
# Below are individual accessors, but should we create an individual
# generic accessor for "other" content?

function get_commentaries($issue) {
    return get_content($issue, "issues", "commentary", "item_issue_number");
}

function get_conference_reports($issue) {
    return get_content($issue, "issues", "conference_report", "item_issue_number");
}

function get_contents($issue) {
    return get_content($issue, "issues", "contents", "item_issue_number");
}

function get_editorials($issue) {
    return get_content($issue, "issues", "editorial", "item_issue_number");
}

function get_extras($issue) {
    return get_content($issue, "issues", "extras", "item_issue_number");
}

function get_interviews($issue) {
    return get_content($issue, "issues", "interview", "item_issue_number");
}

function get_news($issue) {
    return get_content($issue, "issues", "news", "item_issue_number");
}

function get_obituaries($issue) {
    return get_content($issue, "issues", "obituary", "item_issue_number");
}
