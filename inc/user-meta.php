<?php

function the_user_meta_box() {

    $name = get_the_author_meta( 'display_name' );
    $biog = nl2br(get_the_author_meta( 'description'));
    $twitter_handle = get_the_author_meta( 'twitter' );
    $orcid_id = get_the_author_meta( 'orcid' );
    $homepage = get_the_author_meta( 'user_url' );
    $editor_since = get_the_author_meta( 'editor_since' );
    $editor_until = get_the_author_meta( 'editor_until' );
    
    $links =  [
	'twitter' => [
	    'title' => 'Twitter',
	    'input' => $twitter_handle,
	    'url' => 'https://www.twitter.com/' . $twitter_handle,
	    'text' => '@' . $twitter_handle
	],
	'orcid' => [
	    'title' => 'ORCID',
	    'input' => $orcid_id,
	    'url' => 'https://orcid.org/' . $orcid_id,
	    'text' => $orcid_id
	],
	'homepage' => [
	    'title' => 'Homepage',
	    'input' => $homepage,
	    'url' => $homepage,
	    'text' => $homepage
	]
    ];

    if ( $biog ) { echo '<p class="contributor-biog">' . $biog . '</p>'; }
    
    if ( $editor_since ) {
	// Print something if this is or was an editor
	echo '<p>';

	if ( !$editor_until ) {
	    // Current editor
	    echo $name . ' has been an editor of Radical Philosophy since RP' . $editor_since . '.';
	} else {
	    // Past editor
	    echo $name . ' was an editor of Radical Philosophy from RP' . $editor_since . ' to RP' . $editor_until . '.';
	}
	echo '</p>';
    }
    
    foreach ( $links as $link ) {
	if ( $link['input'] != '' ) {
	    echo '<p class="contributor-' . strtolower( $link['title'] ) . '"><span class="title">' . $link['title'] . '</span> : <a href="' . $link['url'] . '" target="_link" title="">' . $link['text'] . '</a>';
	}
    }
}
?>