<?php /* Template Name: Contributors List */

function get_authors_by_letter($first_letter) {
    // Return an array of authors whose lastname begins with $FIRST_LETTER.
    $meta_query_args = array(
	'relation'	=> 'AND',
	array(
	    'key'     => 'nickname',
	    'value'   => "(?:^|[ ])" . $first_letter . "[a-zA-Z-.’']*$",
	    'compare' => 'REGEXP'
	)
    );
    $meta_query = new WP_Meta_Query( $meta_query_args );

    $user_args = array(
	'meta_key'		=> 'last_name',
	'orderby'		=> 'meta_value',
	'order'			=> 'asc',
	'meta_query'            => $meta_query_args,
	'fields'                => ['ID', 'display_name']
    );

    $users = get_users( $user_args  );
    return $users;
}

function get_authors_by_alphabet() {
    /* Returns a multidimensional array of authors. Letters of
       alphabet are array of keys to which array of authors are values. */
    
    $authors = [];
    foreach(range('A', 'Z') as $letter) {
	array_push($authors,
	 	   [ $letter => get_authors_by_letter($letter) ]);
    }
    return $authors;
}

function the_authors_with_meta() {
    /* Prints letters which have authors to them, and then authors as
     * links to respective author pages. */
    $letters = get_authors_by_alphabet();
    $author_count = 0;
    $content = "<div class='contributor-list'>";
    $rp1_url = get_permalink(1187);

    foreach ($letters as $section) {
	foreach ($section as $letter => $authors) {
	    if (!empty($authors)) {
		$content .= "<h2 id='$letter'>" . strtoupper($letter) . "</h2>";
		$content .= "<ul>";
		foreach ($authors as $author) {
		    $name = $author->display_name;
		    $id = $author->ID;
		    $author_page = get_author_posts_url($id);
		    $author_count = $author_count + 1;

		    $content .= "<li><a href='$author_page'>$name</a></li>";
		}
		$content .= "</ul>";
	    }
	}
    }

    $content .= "</div>";

    echo "<p>Since our <a href='$rp1_url'>first issue in 1972</a>, we have published writings by $author_count contributors. If you would like to join them please see our <a href='/submissions'>submissions</a> page.</p>";

    echo get_contributor_letter_links();

    echo $content;
}

function get_contributor_letter_links() {
    /* Prints a list of letters linked to each section of the
       contributor list */
    $content = '<p class="sticky-top bg-white padding-v-small">';
    $content .= 'Navigate to letter';
    $content .= '<span> ';
    foreach(range('A', 'Z') as $letter) {
	$content .= "&nbsp;<a href='#$letter'>$letter</a>";
    }
    $content .= " <a href='#' class='' title='Back to top'>^</a>";
    $content .= '</span>';
    $content .= '</p>';

    return $content;
}

?>

<?php get_header(); ?>

<header class="page-header"><h1 class="page-title"><?php wp_title(''); ?></h1></header>

<?php the_authors_with_meta(); ?>

<?php get_footer(); ?>
