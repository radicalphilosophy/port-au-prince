<?php
/**
 * Template part for displaying RP ITEM PAGES
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package port-au-prince
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('piece-of-writing'); ?>>
    <header class="entry-header">
        <?php
        if ( is_single() ) {
            if ( in_category('issues') ) {
		the_issue_title( '<h1 class="entry-title">', '</h1>' );
            } else {
		the_title( '<h1 class="entry-title">', '</h1>' );
            }
        } else {
            if ( in_category('issues') ) {
		the_issue_title('<h2 class="entry-title"><a href="'. esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>');
            } else {
		the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
            }
        }

	if ( 'post' === get_post_type() ) : ?>
	<div class="entry-meta">
	    <?php 
	    echo get_item_subtitle("h3", "subtitle");
	    echo get_item_reviewed_book("h3", "subtitle", "Review of ");
	    the_item_author_links();
	    
	    ?>
	</div><!-- .entry-meta -->
		<?php
		endif; ?>
    </header><!-- .entry-header -->

    <?php the_pdf_embed(); ?>

    <aside>
	<div>
	<?php the_post_figure('medium');
	the_translator_links('<p class="byline translators">Translated by ', '</p>', false);
	echo get_item_dossier_title('p', 'strong uppercase');
	echo "<p>" . get_item_issue_link("span", "issue-link") . " ~ " . get_item_category_links() . "</p>";
	the_translation_links('span', 'byline', 'Translation(s): ' );
	the_author_biogs();
	the_errata();
	echo "<p>" . get_item_citation() . " (<a class='strong' href='" . get_pdf_url() . "'>pdf</a>)</p>";
	?>
	</div>
    </aside>

    <div class="entry-content">
	<?php
	$issue = get_field('field_5918636bc19fe');
	$item_html_force_show = get_field('field_91gbaw41ytb40');
	$category = get_the_category()[0]->slug;

	echo "<div id='pdf-byline'>" . get_pdf_link() . " " . get_purhcase_issue_link() . "</div>";

	/* If item is not locked or the user is logged in display all the content and a pdf */
	if ( ( $issue->post_title <= 81 || "contents" == $category ) && ( ! $item_html_force_show )) {

	    /* The OCRing of issues 1-81 and contents pages is awful,
	       so do not display content unless the switch to force
	       its showing has been set. */

	} else {
	    the_content( sprintf(
		/* translators: %s: Name of current post. */
		wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'port-au-prince' ), array( 'span' => array( 'class' => array() ) ) ),
		the_title( '<span class="screen-reader-text">"', '"</span>', false )
	    ) );
	    wp_link_pages( array(
		'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'port-au-prince' ),
		'after'  => '</div>',
	    ) );
	}

        ?>
    </div><!-- .entry-content -->

    <footer class="entry-footer">
	<?php port_au_prince_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->
