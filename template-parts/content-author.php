<?php
/**
 * Template part for displaying RP AUTHOR ARCHIVE PAGES
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package port-au-prince
 */

?>

<?php the_archive_item(); ?>
