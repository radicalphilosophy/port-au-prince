<?php
/**
 * Template part for displaying RP ARCHIVE ENTRIES, such as /category/article
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package port-au-prince
 */

?>

<?php the_archive_item(); ?>

