$(function(){
	// trying to clear up conflicts with styles.css
	$('#site-navigation').removeClass('main-navigation');
	
	// add first img in dossier article as first child of dossier, then positioning and styling in css
	var dossierBG = $('#dossier img').first().attr('src');
//	$('#dossier').css('background-image', 'url(' + dossierBG + ')');
	$('#dossier').prepend('<img src="' + dossierBG + '">');
	
	// remove editorial if no item
	if($('#editorial .item').length === 0){
		$('#editorial').hide();	
	}

	// remove featured archive if no item
	if($('#featured-archive .item').length === 0){
		$('#featured-archive').hide();	
	}
	
	// add overflow if more than 6 reviews 
	var reviewsCount = $('#reviews .item').length;
	if (reviewsCount > 6) {
		$('#reviews').addClass('overflow');
	}
	
	// adjust features layout if odd number
	var featuresCount = $('#features .item').length;
	if (featuresCount % 2 === 0) {
		$('#features .item').addClass('even');
	}
	
	// brackets to footnotes that are missing them
	$('.footnoteRef').each(function(){
		if($(this).is(':contains("[")')) {
		} else {
			$(this).addClass('bracket');			
		}
	});
	// swap new footnotes image
//	$('.footnotes img').attr('src', '/wp-content/themes/port-au-prince/icon-footnote.png');
});
