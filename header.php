<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package port-au-prince
 */

?>
<!DOCTYPE html>
<?php
if (is_single() && (!in_category('issues'))) {
    // If this is an item page
    echo "<html lang='en-GB' prefix='og: http://ogp.me/ns/article#'>\n";
} else {
    // If it's anything else
    echo "<html lang='en-GB' prefix='og: http://ogp.me/ns/website#'>\n";
}
?>
<head>
    <?php
    // If we're not on production, ask crawlers to not index
    if (false == strpos(home_url(), 'www.radicalphilosophy.com')) {
	echo "<!-- Please don't index this site! --><meta name='robots' content='noindex'>\n";
    }

    the_rp_metadata();
    include 'inc/favicons.php';
    the_analytics_snippet();
    wp_head();
    ?>

    <link href="/wp-content/themes/port-au-prince/dist/css/styles.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		 integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
		 crossorigin="anonymous"></script>
    <script type="text/javascript" src="/wp-content/themes/port-au-prince/dist/js/index.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Overpass:400,700,900|Crimson+Pro&display=swap" rel="stylesheet" />

    <style type="text/css">
     <?php the_alert_banner_style(); ?>
    </style>
</head>

<body <?php body_class(); ?>>
    <?php echo the_alert_banner(); ?>
    <div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'port-au-prince' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
	    <div class="site-branding">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="/wp-content/themes/port-au-prince/img/rp-horiz.svg" /></a>
	    </div><!-- .site-branding -->

	    <nav id="site-navigation" class="main-navigation" role="navigation">
                <ul class="menu">
                    <li>
                        <!-- First Tier Drop Down -->
                        <span href="/category/issues">Archive</span>
                        <ul>
                            <li><a href="/category/issues">Issues</a></li>
			    <li><a href="/contributors">Contributors</a></li>
			    <li><a href="/category/article">Articles</a></li>
                            <li><a href="/category/commentary">Commentaries</a></li>
                            <li><a href="/category/reviews">Reviews</a></li>
                            <li><a href="/category/interview">Interviews</a></li>
                            <li><a href="/category/obituary">Obituaries</a></li>
                        </ul>
                    </li>
                    <li>
                        <!-- First Tier Drop Down -->
                        <span><a href="/about">About</a></span>
                        <ul>
			    <li><a href="/editorial/201-editorial">Mission statement</a></li>
                            <li><a href="/print">Print edition stockists</a></li>
			    <li><a href="/submissions">Submissions </a></li>
			    <li><a href="https://twitter.com/RPhilos/" class="nav-twitter" <?php echo get_plausible_event("twitter"); ?>>Twitter</a></li>
			    <li><a href="https://www.facebook.com/RadicalPhilosophyMag/" class="nav-fb" <?php echo get_plausible_event("facebook"); ?>>Facebook</a></li>
                        </ul>
                    </li>
		    <li>
                        <!-- First Tier Drop Down -->
                        <span>Support</span>
                        <ul>
			    <li><a href="https://www.patreon.com/radicalphilosophy" <?php echo get_plausible_event("patreon"); ?>>Patreon</a></li>
                            <li><a href="/supporters">Supporters</a></li>
                        </ul>
                    </li>
		    <li>
			<?php get_search_form(); ?>
		    </li>
                </ul>
	    </nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
