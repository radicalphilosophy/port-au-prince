<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package port-au-prince
 */

get_header(); ?>
<!-- Track 404s on Plausible -->
<script>plausible("404",{ props: { path: document.location.pathname } });</script>
<!-- End Plausible -->

<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
				    <h1 class="page-title"><?php esc_html_e( '404: Page not found...', 'port-au-prince' ); ?></h1>
				</header><!-- .page-header -->

				<div class="entry-content">

				    <p>Perhaps a search might help?</p>

				    <p><?php get_search_form(); ?></p>

				    <p>Or here some of our commonly
				    used tags?</p>
				    <p><?php wp_tag_cloud( 'smallest=10&largest=10&number=75' ) ?></p>
				</div><!-- .entry-content -->

        		</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
