<?php
/**
 * The template for displaying RP ISSUE PAGES
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package port-au-prince
 */

get_header(); ?>

	<div id="primary" class="content-area">
	    <main id="wp_main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

                /* Is this an issue? */
                if ( in_category("10") ) {
		    the_rp_issue(); ?>
		    
                    <div id="features">
                        <!-- Featured Items -->
                        <?php featured_articles(get_post()); ?>
                    </div>
                    <div id="main">
                        <!-- Dossier -->
                        <?php dossier(get_post()); ?>
                        <!-- Each other item -->
                        <?php other_articles(get_post()); ?>
                    </div>
                    <div id="side">
                        <!-- Editorial -->
                        <?php editorial(get_post()); ?>
                        <!-- Reviews -->
                        <?php reviews(get_post()); ?>
                        <!-- Featured Archive -->
                        <?php featured_archive(get_post()); ?>
                    </div>
                <?php } else {
                    get_template_part( 'template-parts/content', get_post_format() );
                }

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
