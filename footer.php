<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package port-au-prince
 */

?>

            </div><!-- #content -->

            <footer id="colophon" class="site-footer" role="contentinfo">
                <div id="footer-head">
                    <a href="/"><span id="footer-title">radical philosophy</span></a>
                    <div id="social">
                        <a href="<?php echo get_social_info()["facebook"]["url"];?>" target="_blank" <?php echo get_plausible_event("facebook"); ?>>
                            <img src="<?php echo get_social_info()["facebook"]["icon30x30"];?>" width="30" height="30" title="Connect with us on Facebook" border="0"/>
                            <span class="text">Facebook</span>
                        </a>
                        &nbsp;
                        <a href="<?php echo get_social_info()["twitter"]["url"];?>"  <?php echo get_plausible_event("twitter"); ?>>
                            <img src="<?php echo get_social_info()["twitter"]["icon30x30"];?>" width="30" height="30" title="Connect with us on Twitter" style="border: none;"/>
                            <span class="text">Twitter</span>
                        </a>
                        &nbsp;
                        <a href="<?php bloginfo('rss2_url')?>"  <?php echo get_plausible_event("rss"); ?>>
                            <img src="<?php echo get_social_info()["feed"]["icon30x30"];?>" width="30" height="30" title="Follow our RSS feed" style="border: none;"/>
                            <span class="text">RSS</span>
                        </a>
                    </div> <!-- #footer-social -->
                </div> <!-- #footer-head -->
		
		<div id="footer-s2">
                    <a href="https://www.radicalphilosophyarchive.com/">PDFs of issues 1-200 hosted at <b>radicalphilosophyarchive.com</b></a></br>
                    <a href="https://creativecommons.org/licenses/by-nc-nd/3.0/">Content license (<?php echo date("Y");?>): Creative Commons BY-NC-ND</a><br/>
		    <a href="https://www.bytemark.co.uk/">Website hosting supported by <b>:BYTEMARK</b></a>
                </div>
            </footer><!-- #colophon -->
        </div><!-- #page -->

        <?php wp_footer(); ?>

	<script>
	 // Plausible events handler
	 let elements=document.querySelectorAll("a[data-analytics]");registerAnalyticsEvents(elements,handleLinkEvent);elements=document.querySelectorAll("button[data-analytics]");registerAnalyticsEvents(elements,handleFormEvent);function registerAnalyticsEvents(elements,callback){for(var i=0;i<elements.length;i+=1){elements[i].addEventListener('click',callback);elements[i].addEventListener('auxclick',callback)}}function handleLinkEvent(event){var link=event.target;var middle=event.type=="auxclick"&&event.which==2;var click=event.type=="click";while(link&&(typeof link.tagName=='undefined'||link.tagName.toLowerCase()!='a'||!link.href)){link=link.parentNode}if(middle||click){registerEvent(link.getAttribute('data-analytics'))}if(!link.target){if(!(event.ctrlKey||event.metaKey||event.shiftKey)&&click){setTimeout(function(){location.href=link.href},150);event.preventDefault()}}}function handleFormEvent(event){event.preventDefault();registerEvent(event.target.getAttribute('data-analytics'));setTimeout(function(){event.target.form.submit()},150)}function registerEvent(data){let attributes=data.split(/,(.+)/);let events=[JSON.parse(attributes[0]),JSON.parse(attributes[1]||'{}')];plausible(...events)}
	</script>
    </body>
</html>
