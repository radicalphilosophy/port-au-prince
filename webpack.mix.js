
// Process SCSS and JS using laravel-mix.
//
// make sure you have webpack installed, if not
// run `npm install --save-dev webpack laravel-mix`
//
// Then simply run `npx mix` or `npx mix watch` to autoreload browser
// on changes
//
// Note this does NOT automatically MINIFY files. To do so you need to
// set NODE_ENV to production. Ie `npx mix --production`

let mix = require('laravel-mix');

// Set this to wherever you run your local version of the website
let devEnv = 'http://localhost';

mix.setPublicPath('dist')                // Set path of output
    .js('src/js/index.js', 'js')         // Process JS
    .sass('src/scss/styles.scss', 'css') // Process SCSS
    .browserSync(devEnv);                // Requires `watch`
