<?php
/**
 * port-au-prince functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package port-au-prince
 */

function get_issue_id() {
    // Depending on our context, our ID will be in the ether.
    $category = get_the_category($post->ID);
    if ( !is_single() || "issues" ==$category[0]->slug ) {
        return get_the_ID();
    } else {
        return get_field('field_5918636bc19fe')->ID;
    }
}

function get_plain_issue_number() {
    return get_field('field_5918636bc19fe')->post_title;
}

function get_issue_date($issue, $result = 'my') {
    $year = get_field('field_58eaaaf246d9b', $issue);
    $month = get_field('field_58eaab453f8dd', $issue);
    return "$month $year";
}

function get_styled_issue($title) {
    if ( $title < 201 ) {
        return $title;
    } else {
        $s = substr($title, 0, 1);
        $n = substr($title, 1);
        return $s . "." . $n;
    }
}

function get_item_subtitle($tag = "span", $class, $before = "", $after = "") {
    $subtitle = get_field('item_detail_sub_title', false);

    return ($subtitle) ? "<$tag class='$class'>" . $before . $subtitle . "</$tag>" : "";
}

function get_item_dossier_title($tag = 'span', $class = '', $prefix = 'Dossier: ') {
    // todo: extend this to link to dossier or unfold all pieces?
    $dossier = get_field('field_591857e895c8b');
    if ($dossier) {
	$title = $dossier->post_title;
	return "<$tag class='$class'>$prefix$title</$tag>";
    };
}

function get_item_reviewed_book($tag = "span", $class, $before = "", $after = "") {
    $book = get_field('field_3929en29dj2i', false);

    return ($book) ? "<$tag class='$class'>" . $before . $book . "</$tag>" : "";
}

function get_item_issue_link($tag = "span", $class, $before = "", $after = "") {
    if ( !in_category('issues') ) {
        $issue = get_field('field_5918636bc19fe');
	$url = get_permalink($issue->ID);
	$issue_number = get_styled_issue($issue->post_title);
	$date = get_issue_date($issue);

        return "<$tag class='$class'>$before<a href='$url'>RP $issue_number ($date)</a>$after</$tag>";
    }
}

function get_item_category_links($class = "") {
    $categories = get_the_category();
    $output = "";

    foreach ($categories as $category) {
	$cat_title = $category->name;
	$cat_slug =  get_site_url() . '/category/' . $category->slug;

	$output .= "<a class='$class' href='$cat_slug'>$cat_title</a> ";
    }

    return $output;
}

function get_item_page_range() {
    $page_start = get_field('field_5c5d939907c84');
    $page_end = get_field('field_5c5d93e307c85');

    if ($page_start && $page_end) {
	if ($page_start == $page_end) {
	    return "p. $page_start";
	} else {
	    return "pp. $page_start&ndash;$page_end";
	}
    }
}

function get_item_citation() {
    $issue = get_field('field_5918636bc19fe');
    $date = get_issue_date($issue);
    $title = get_the_title();
    $subtitle = (get_field('item_detail_sub_title', false));
    $subtitle = ($subtitle) ? ": " . strip_tags($subtitle, '<em><i>') : "";
    $authors = coauthors(null, " and ", null, null, false);
    $issue = get_field('field_5918636bc19fe', false)->post_name;
    $pages = get_item_page_range();
    $pages = ($pages) ? ", $pages" : "";
    $translators = the_translator_links(null, null, false, false);
    $translators = ($translators) ? ", trans. $translators": "";

    return "$authors, '$title$subtitle'$translators, <em>Radical Philosophy</em> $issue, $date$pages.";
}

function the_item_citation() {
    echo get_item_citation();
}

function the_archive_item() {
    $id = get_the_ID();
    $url = esc_url(get_permalink());
    $title = get_the_title();
    $subtitle = get_item_subtitle('span', null);
    $subtitle = ($subtitle) ? ": " . strip_tags($subtitle, '<em><i>') : "";
    $authors = coauthors_posts_links(null, null, null, null, false);
    $category = get_item_category_links();
    $category_name = get_the_category()[0]->slug;
    $issue_link = get_item_issue_link("span", "issue-link");
    $pages = get_item_page_range();
    $pages = ($pages) ? ", $pages" : "";
    $reviewed_book = get_item_reviewed_book("div", "subtitle", "Reivew of ");
    $dossier_title = get_item_dossier_title('p', 'uppercase');
    $thumbnail =  get_the_post_thumbnail(null, 'thumbnail');
    $post_class =  get_post_class('archive-page');
    $excerpt = get_the_excerpt();
    $excerpt = ( "contents" == $category_name ) ? "" : get_the_excerpt();
    
    echo "
<article id='post-$id' class='archive-entry'>
    <header class='entry-header'>

        <h3 class='uppercase'><a href='$url'>$title$subtitle</a></h3>
        <div class='uppercase'>$dossier_title</div>
        <div>$reviewed_book</div>
        <div><span class='strong'>$authors</span> ~ $issue_link$pages ~ $category</div>
        <div class='excerpt'>$excerpt</div>
    </header>
    $thumbnail
</article>";

}

function get_issue_image($issue, $size) {
    if ( has_post_thumbnail($issue) ) {
	// if this thing has a thumbnail, then return that
        return get_the_post_thumbnail_url($issue, $size);
    } else {
	return site_url() . "/wp-content/files_mf/" . get_field('issue_detail_cover_image', $issue);
    }
}

function the_issue_image($issue, $size) {
    echo '<img src="'. get_issue_image($issue, $size) . '" \>';
}

function the_issue_title($pre, $post, $print = true ) {
    $title = get_the_title();
    $date = get_issue_date();
    
    $string = "$pre RP $title ($date) $post";

    if ( $print ) :
    echo $string;
    else :
    return $string;
    endif;
}

function get_plausible_event($event) {
    /* Returns a Plausible analytics snippet */

    switch ($event) {
	case "pdf":
	    $name = 'PDF download';
	    break;
	case "patreon":
	    $name = 'Exit / Patreon';
	    break;
	case "twitter":
	    $name =  'Exit / Twitter';
	    break;
	case "facebook":
	    $name = 'Exit / Facebook';
	    break;
	case "rss":
	    $name = 'Signup / RSS';
	    break;
	default:
	    $name = false;
    }

    return "data-analytics='$name'";
}

function get_social_info() {
    /* 
     *     Easily retrieve social media information
     * 
     *     Example usage:
     *     echo get_social_info()["facebook"]["url"];
     *     echo get_social_info()["twitter"]["handle"];
     *     echo get_social_info()["feed"]["icon30x30"];
     * 
     *     Note feed information (uris etc) should be retrieved via bloginfo()
     */
    return array(
	'facebook' => array(
            'id' => '130391006992138',
            'name' => 'RadicalPhilosophyMag',
            'url' => esc_url( 'https://www.facebook.com/RadicalPhilosophyMag/' ),
            'icon30x30' => esc_url( get_template_directory_uri() . '/img/facebook30x30.png' )
	),
	'twitter' => array(
            'handle' => '@RPhilos',
            'url' => esc_url( 'https://twitter.com/RPhilos/' ) ,
            'icon30x30' => esc_url( get_template_directory_uri() . '/img/twitter30x30.png' )
	),
	'feed' => array(
            'icon30x30' => esc_url( get_template_directory_uri() . '/img/rss30x30.png' )
	)
    );
}

// Define length of excerpt for locked items and archive/search pages
function custom_excerpt_length( $length ) {
    if (is_single()) {
        return 700; // for items
    } else {
	return 55; // for archive and search pages
}}
add_filter( 'excerpt_length', 'custom_excerpt_length' );

function the_rp_issue() {

    if ( is_home() || is_front_page() ) {
	$issue = get_latest_issue();
    } elseif ( !is_single() ) {
	printf($xml->asXml());
	return;
    } else {
	$post = get_post();
	$category = get_the_category($post->ID);
	if ( "issues" == $category[0]->slug ) {
            $issue = $post;
	} else {
            $issue = get_field("field_5918636bc19fe", get_post()->ID);
	}
    }
    $num = $issue->post_title;
    $styled_num = get_styled_issue($num);
    $date = get_rpdate($issue);

    if ( !empty(get_the_post_thumbnail($issue->ID)) )  {
	$image = get_the_post_thumbnail($issue->ID, "medium");
    } else {
	$file =  get_field('field_58eaaa02cea31', $issue->ID);
	$image = "<img alt='Radical Philosophy $styled_num jacket' src='/wp-content/files_mf/$file'>";
    }

    echo "<div class='rp-issue-cover'>
    $image
    <div>RP $styled_num ($date)</div>
    </div>";
}

function get_pdf_url() {
    $issue = get_field('field_5918636bc19fe');
    $pdf_rpa_override = get_field('field_58ekac41fab3e');
    if ( ($issue->post_title <= 200) && ( $pdf_rpa_override != true ) ) {
	$archive_pdf_uri = "https://www.radicalphilosophyarchive.com/issue-files/";
	$pdf_filename = get_field('field_58eaac4afdb41')["filename"];
	return $archive_pdf_uri . $pdf_filename;
    } else {
	return get_field('field_58eaac4afdb41')["url"];
    }
}

function get_pdf_link () {
    $pdf = get_pdf_url();
    $data_analytics = get_plausible_event('pdf');

    if ( $pdf ) {
	// if a pdf link is known about, link to it
        return "<a class='pdf-download-link' href='$pdf' $data_analytics >Download pdf</a>";
    }
}

function get_purhcase_issue_link() {
    $issue = get_field('field_5918636bc19fe');
    if ( $issue->post_title > 200 ) {
	// If we're on a series 2 issue, display link to print purchase page
	return '~ <a href="/print">Purchase issue</a>';
    }
}

function the_pdf_embed() {
    $pdf_url = get_pdf_url();

    echo "<object data='$pdf_url' type='application/pdf' ></object>";
}

function the_post_figure($size) {
    if ( has_post_thumbnail() ) {
	$post_id = get_post()->id;

	$thumbnail_id = get_post_thumbnail_id();
	$thumbnail_tag = get_the_post_thumbnail( $post_id, $size, ['margin' => '0'] );

	$figcaption = get_post($thumbnail_id)->post_excerpt;
	$figcaption_tag = ($figcaption) ? "<figcaption class='wp-caption-text'>$figcaption</figcaption>": "";

	echo "<figure id='attachment_$thumbnail_id' class='wp-caption alignnone'>$thumbnail_tag $figcaption_tag</figure>";
    }
}

if ( ! function_exists( 'port_au_prince_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function port_au_prince_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on port-au-prince, use a find and replace
     * to change 'port-au-prince' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'port-au-prince', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'menu-1' => esc_html__( 'Primary', 'port-au-prince' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    // Set up the WordPress core custom background feature.
    add_theme_support( 'custom-background', apply_filters( 'port_au_prince_custom_background_args', array(
        'default-color' => 'ffffff',
        'default-image' => '',
    ) ) );

    // Add theme support for selective refresh for widgets.
    add_theme_support( 'customize-selective-refresh-widgets' );

    // Remove all capabilities from authors
    $author = get_role( 'author' );
    foreach ($author->capabilities as $k => $v) {
        $author->remove_cap( $k );
    }
    $author->add_cap( 'edit_posts' );
}
endif;
add_action( 'after_setup_theme', 'port_au_prince_setup' );

/*
 * Relevanssi filters
 */
add_filter('query_vars', 'add_post_author');
function add_post_author($qv) {
    $qv[] = 'post_author';
    return $qv;
}
add_filter('relevanssi_hits_filter', 'post_author_filter');
function post_author_filter($hits) {
    global $wp_query;
    if (isset($wp_query->query_vars['post_author'])) {
        $valid_hits = array();
        foreach ($hits[0] as $hit) {
            if (in_array($wp_query->query_vars['post_author'], wp_get_object_terms($post->ID, 'author', array('fields' => 'ids')))) {
                $valid_hits[] = $hit;
            }
        }
        $hits[0] = $valid_hits;
    }
    return $hits;
}
add_filter('relevanssi_match', 'category_weight');
function category_weight($match) {
    $category = get_the_category($match->doc);
    $category_id = $category[0]->cat_ID;
    if ($category_id == '22'){ // reviews
    	$match->weight = $match->weight / 10;
    }
    elseif ($category_id == '3') { // orbituary
    	$match->weight = $match->weight * 1;
    }
    elseif ($category_id == '4') { // article
        $match->weight = $match->weight * 2;
    }
    else {
    }
    return $match;
}

function remove_menu_entries(){
    if ( function_exists( 'remove_menu_page' ) ) {
        remove_menu_page( 'link-manager.php' );
    }
}
add_action( 'admin_menu', 'remove_menu_entries' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function port_au_prince_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'port_au_prince_content_width', 640 );
}
add_action( 'after_setup_theme', 'port_au_prince_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function port_au_prince_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'port-au-prince' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'port-au-prince' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'port_au_prince_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function the_analytics_snippet() {
    /* Return Plausible analytics snippet if on radicalphilosophy.com domain */
    $urlparts = parse_url( home_url());
    $domain = $urlparts['host'];

    if ('www.radicalphilosophy.com' == $domain) {
        echo '<!-- Plausible analytics snippet -->
	      <script defer data-domain="radicalphilosophy.com" src="https://plausible.io/js/plausible.outbound-links.js"></script>
	      <!-- Plausible event tracking snippet. See https://plausible.io/docs/custom-event-goals -->
	      <script>window.plausible = window.plausible || function() { (window.plausible.q = window.plausible.q || []).push(arguments) }</script>
	      <!-- End plausible snippets -->';
    } elseif ('staging.radicalphilosophy.com' == $domain) {
        echo '<script defer data-domain="staging.radicalphilosophy.com" src="https://plausible.io/js/plausible.js"></script>';
    } else {
        echo '<!-- No analytics snippet loaded -->';
    }
}

function disable_emojis() {
    // Remove unnecessary Emoji autodetection script
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
}
add_action( 'init', 'disable_emojis');

/**
 * Make the default editor default to plain html instead of WYSWYG.
 */
function show_html_editor() {
    return 'html';
}
add_filter( 'wp_default_editor', 'show_html_editor' );

/**
 * Display any official amendments to an item, if they exist.
 */
function the_errata() {
    // check if the repeater field has rows of data
    if( have_rows('amendment') ) {

	echo '<section class="errata">';

	while ( have_rows('amendment') ) : the_row();

	$text = get_sub_field('amendment_description', false);
	$date = get_sub_field('amendment_date');

	echo "<p><b>Erratum:</b> " . $text . " ($date)</p>";

	endwhile;
	echo '</section>';
    } else {
	// no rows found
    }
}

function the_author_biogs() {
    // Print author biogs, if they exist
    $biogs = get_field('item_author_biogs');

    if ($biogs) {
	echo "<div class='author-biogs'>$biogs</div>";
    }
}

function the_print_link() {
    $issue = get_field('field_5918636bc19fe');
    if ( $issue->post_title > 200 ) {
	$copy = "Buy this issue";
    } else {
	$copy = "Buy the latest issue";
    }
    echo '<a href="/print">'. $copy .'</a>';
}

/* List and link to the translators of an item
 *
 * Prepend things, perhaps an opening tag, with $pre
 * Postpend things, perhaps a closing tag, with $post
 * Link to the translator's page (default)
 * Print the results (default) or rather return
 */
function the_translator_links($pre, $post, $link = true, $print = true) {
    $translators = get_field('item_translators');

    if (!empty($translators)) {

	$output = $pre;

	for ($i = 0; $i < count($translators); $i++) {

	    $fname = $translators[$i]['user_firstname'];
	    $lname = $translators[$i]['user_lastname'];

	    if ($link) {
		$url   = "/author/" . $translators[$i]['user_nicename'];
		$output .= '<a href="' . $url . '">';
	    }

	    $output .= $fname . ' ' . $lname;

	    if ($link) {
		$output .= '</a>';
	    }

	    if ($i + 1 == count($translators) - 1) {
		$output .= " and ";
	    } elseif ( $i + 1 != count($translators) ) {
		$output .= ", ";
	    }
	}

	$output .= $post;


	if ($print) :
	echo $output;
	else :
	return $output;
	endif;
    }
}

/* Links to translations of items on other websites */
function the_translation_links($tag = "span", $class = "", $pre = null, $post = null, $spacer = " ") {

    $translations = get_field('item_translations');

    if (!empty($translations)) {

	$link = "<$tag class='$class'>" . $pre;

	foreach ($translations as $friend) {
	    $title = $friend['item_translation_pub_name'];
	    $lang = $friend['item_translation_language'];
	    $url = $friend['translation_url'];

	    $link .= "$spacer <a href='$url'><em>$title</em> ($lang)</a>";
	}

	$link .= $post . "</$tag>";

	echo $link;
    }
}

function alert_banner_active(){
    // Test if alert banner should be displayed

    $alert_content = get_field('alert_banner_content', 'option');
    $alert_start = get_field('alert_banner_start_date', 'option');
    $alert_end = get_field('alert_banner_end_date', 'option');
    $today = date('Ymd');

    if ($alert_content && $alert_end && $today >= $alert_start && $today <= $alert_end ) {
	return true;
    } else {
	return false;
    }
}

function the_alert_banner(){
    // Display an alert banner at the top of the website.

    if (alert_banner_active()) {
	$alert_content = strip_tags(get_field('alert_banner_content', 'option'), '<a><em><b><i><strong><br>');
	echo '<div id="alert-banner"><span>' . $alert_content . '</span></div>';
    }
}

function the_alert_banner_style(){
    // Print relevant alert banner styles

    if (alert_banner_active()) {
	$alert_banner_bg_color = esc_html(get_field('alert_banner_bg_color', 'option'));
	$alert_banner_text_color = esc_html(get_field('alert_banner_text_color', 'option'));
	$alert_banner_a_color = esc_html(get_field('alert_banner_a_color', 'option'));

	$bannerstyle  = (isset($alert_banner_bg_color)) ? '#alert-banner { background-color: ' . $alert_banner_bg_color . ';} ': '';
	$bannerstyle .= (isset($alert_banner_text_color)) ? '#alert-banner span { color: ' . $alert_banner_text_color . ';} ': '';
	$bannerstyle .= (isset($alert_banner_a_color)) ? '#alert-banner span a { color: ' . $alert_banner_a_color . ';} ': '';
	echo $bannerstyle;
    }
}

function modify_user_contact_methods( $user_contact ) {
    // Add user contact methods
    $user_contact[ 'twitter' ] = __( 'Twitter handle (no @)' );
    $user_contact[ 'orcid' ] = __( 'ORCID' );

    /* Ok so these aren't exactly contact methods, but if we create a
       new section for them it'll have to go at the foot of the page
       (by means of an action hooked to show_user_profile and
       edit_user_profile ) */
    $user_contact[ 'editor_since' ] = __( 'RP editor since issue number' );
    $user_contact[ 'editor_until' ] = __( 'RP editor until issue' );

    // Remove user contact methods
    unset( $user_contact['aim'] );
    unset( $user_contact['yim'] );
    unset( $user_contact['jabber'] );

    return $user_contact;
}
add_filter( 'user_contactmethods', 'modify_user_contact_methods' );


function var_dumpp($query) {
    echo '<pre>';
    var_dump($query);
    echo '</pre>';
}

function dvd($query) {
    die(var_dumpp($query));
}

function the_item_author_links() {
    if ( function_exists('coauthors_posts_links') ){
	coauthors_posts_links(null, " and ", "<span class='author-byline byline'>", "</span>", true);
    } else {
        get_the_author_posts_link();
    }
}

function the_plain_authors() {
    $authors = get_coauthors();
    if ($authors) {
	foreach($authors as $author) {
	    echo "$author->display_name . ";
	}
    }
}

function get_plain_tags( $sep = ", ") {
    $posttags = get_the_tags();
    $tags = "";
    if ($posttags) {
	foreach($posttags as $tag) {
	    $tags .= $tag->name . $sep;
	}
    }
    return $tags;
}

function get_start_page() {
    return get_field('pdf_page_start');
}

function get_end_page() {
    return get_field('pdf_page_end');
}

function limit_text($text, $limit) {
    // Return a $LIMIT amount of words from $TEXT followed by an elipses.
    if (str_word_count($text, 0) > $limit) {
	$words = str_word_count($text, 2);
	$pos   = array_keys($words);
	$text  = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}
/**
 * Add Author creator functionality
 */
require get_template_directory() . '/inc/author-creator.php';

/**
 * Add our custom Admin UI Tweaks
 */
require get_template_directory() . '/inc/admin-ui-tweaks.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load ACF definitions
 */
require get_template_directory() . '/inc/advanced-custom-fields-dossiers.php';
require get_template_directory() . '/inc/advanced-custom-fields-issues.php';
require get_template_directory() . '/inc/advanced-custom-fields-items.php';
require get_template_directory() . '/inc/advanced-custom-fields-menu.php';

/**
 * Add LaTeX conversion machine to admin panel.
 */
require get_template_directory() . '/inc/texmachine.php';

/**
 * Functions for interacting RP data abstractions: dossiers, issues,
 * articles reviews, etc.
 */
require get_template_directory() . '/inc/rp-datastructures.php';

require get_template_directory() . '/inc/user-meta.php';

require get_template_directory() . '/inc/header-meta.php';
